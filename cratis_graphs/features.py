# coding: utf-8
import os
from cratis.features import Feature


class Grpahs(Feature):

    def configure_settings(self):

        self.settings.TEMPLATE_DIRS += (os.path.dirname(__file__) + '/templates',)
        self.settings.STATICFILES_DIRS += (os.path.dirname(__file__) + '/static',)

