# coding=utf-8
from configurations import values
from cratis.settings import CratisConfig
from cratis_base.features import Common
from cratis_admin.features import AdminArea
from cratis_admin_suit.features import AdminThemeSuit
from cratis_cms.features import Cms
from cratis_filer.features import Filer, FilerCms

import os


class Dev(CratisConfig):
    DEBUG = True
    SECRET_KEY = values.Value('sjahjkshdjakh')

    DATABASES = values.DatabaseURLValue('sqlite://var/db.sqlite3')

    FEATURES = (
        Common(sites_framework=True),
        AdminThemeSuit(title='CMS admin'),
        AdminArea(),

        Cms(),

        Filer(),
        FilerCms(),
    )

    TEMPLATE_DIRS = (
        os.path.join(CratisConfig.BASE_DIR, 'templates'),
    )

    CMS_TEMPLATES = (
        ('template_1.html', 'Template One'),
        ('template_2.html', 'Template Two'),
    )

    LANGUAGES = (
        ('en', 'English'),
    )


class Test(Dev):
    pass


class Prod(Dev):
    DEBUG = False

    FEATURES = Dev.FEATURES + (
        # your features here
    )
