from django.conf.urls import patterns, include
import os
from cratis.features import Feature


class Ckeditor(Feature):

    def configure_settings(self):
        self.append_apps(['ckeditor'])

        self.settings.CKEDITOR_UPLOAD_PATH = "uploads/"

    def configure_urls(self, urls):

        urls += patterns('',
            (r'^ckeditor/', include('ckeditor_uploader.urls')),
        )
