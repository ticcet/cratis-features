from cratis_erply.api import get_client, NoRecords


class ErplyOrderStorage(object):

    def get_user_orders(self, user):
        orders = {
            'all': [],
            'unpaid': [],
            'paid': [],
            'sent': [],
        }

        try:
            for order in get_client().fetch_many('getSalesDocuments', {
                'clientID': user.erply_customer_id,
                'types': 'ORDER,OFFER',
                'getRowsForAllInvoices': 1,
                'orderBy': 'dateAndNumber',
            }):

                if not (not order['paid'] or (float(order['total']) - float(order['paid'])) > 3):
                    order['state'] = 'paid'
                    orders['paid'].append(order)

                elif len(order['followUpDocuments']) > 0:
                    order['state'] = 'sent'
                    orders['sent'].append(order)

                else:
                    order['state'] = 'unpaid'
                    orders['unpaid'].append(order)

                orders['all'].append(order)
        except NoRecords:
            pass

        return orders

    def send_order(self):
        pass

    def payment_success(self, order, amount, po):
        payment = {
                'documentID': po.document_id,
                'type': 'TRANSFER',
                'typeID': 6,
                'sum': amount,
                'currencyCode': order['currency']['code']
        }
        get_client().fetch_one('savePayment', payment)

    def confirm_order(self, region, order, po, paid):

        if paid:
            document = {
                'type': 'ORDER',
                'currencyCode': order['currency']['code'],
                'currencyRate': order['currency']['rate'],
                'customerID': po.user.erply_customer_id,
                'addressID': order['address'],
                'confirmInvoice': 1,
                'paymentType': 'TRANSFER',
                'reserveGoods': 1 if paid else 0,
                'sendByEmail': 1,
                'customNumber': 'GR-%s' % (100000 + po.pk),
                'invoiceState': 'READY',
                'paymentStatus': 'PAID' if paid else 'UNPAID',
            }

        # offline-payment

        else:
            document = {
                'type': 'ORDER',
                'currencyCode': order['currency']['code'],
                'currencyRate': order['currency']['rate'],
                'customerID': po.user.erply_customer_id,
                'addressID': order['address'],
                'confirmInvoice': 1,
                'paymentType': 'TRANSFER',
                'reserveGoods': 0,
                'sendByEmail': 1,
                'customNumber': 'GR-%s' % (100000 + po.pk),
                'invoiceState': 'READY',
                'paymentStatus': 'UNPAID'
            }

        if po.document_id:
            document['invoiceID'] = po.document_id

        last_idx = 0
        for i, item in enumerate(order['items'].values()):
            idx = i + 1
            document['productID%s' % idx] = item['productID']
            document['amount%s' % idx] = item['qty']
            document['price%s' % idx] = item['price'] / float(order['currency']['rate'])
            last_idx = idx

        idx = last_idx + 1
        document['productID%s' % idx] = 8682
        document['amount%s' % idx] = 1
        document['price%s' % idx] = order['totalDelivery'] / float(order['currency']['rate'])

        order_ = get_client().fetch_one('saveSalesDocument', document)

        po.document_id = order_['invoiceID']
        po.sum = order_['total']


