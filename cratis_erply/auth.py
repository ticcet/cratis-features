import hashlib
from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.utils import user_email, user_username, user_field
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from cratis_erply.api import RequestFailed
from django.conf import settings
from django.contrib.auth import get_user_model
from cratis_erply.models import User
from cratis_erply.countries import country_choices, group2country
from django.core.exceptions import ValidationError

from django.core.validators import validate_email

from django.utils.translation import ugettext_lazy as _
from django import forms
from django.utils.crypto import random
from cratis_erply.features import ErplyAuth, ErplyIntegration



class ErplyAuthBackend(object):

    @inject.param('client', 'erply')
    def authenticate(self, username=None, password=None, email=None, client=None):

        if not username and email:
            username = email

        if not email and username:
            email = username

        try:
            user_data = client.login(
                '{0}'.format(username), '{0}'.format(password))
        except RequestFailed:
            return None


        # should be a vaild email
        validate_email(username)

        if user_data:
            try:
                user = User.objects.get(username=username)
            except User.DoesNotExist:
                # just random password to protect user when this auth backend
                # gets disabled get disabled
                user = User(username=username,
                            password=str(
                                hashlib.sha224(str(random.random())).hexdigest()))

            user.email = email
            user.first_name = user_data['clientName']
            user.last_name = user_data['clientLastName']
            user.erply_customer_id = user_data['clientID']
            user.save()

            # change password (for checkout)
            if password:
                user.set_password(password)
                user.save()
            return user

        return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


class SignupForm(forms.Form):
    first_name = forms.CharField(max_length=155, label=_('* Firstname'))
    last_name = forms.CharField(max_length=155, label=_('* Lastname'))
    # country = forms.ChoiceField(choices=country_choices, initial=26)
    company_name = forms.CharField(
        max_length=155, label=_('Company name'), required=False)
    company_reg_nr = forms.CharField(
        max_length=155, label=_('Company reg.nr.'), required=False)
    company_vat_nr = forms.CharField(
        max_length=155, label=_('Company vat.nr.'), required=False)

    def clean(self):
        data = super(SignupForm, self).clean()

        if data.get('company_name') and not data.get('company_reg_nr'):
            ValidationError(
                _('Reg nr for companies is mandatory'), code='invalid')

        return data

    class Meta:
        model = get_user_model()

    def save(self, user):
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']


class AllauthErplySocialAccountAdapter(DefaultSocialAccountAdapter):
    pass


class AllauthErplyAccountAdapter(DefaultAccountAdapter):
    #
    # def clean_email(self, email):
    #    if get_client().user_exists(email):
    #        raise forms.ValidationError(_("This username is already taken. Please "
    #                                      "choose another."))
    #
    #    return super(AllauthErplyAccountAdapter, self).clean_email(email)

    @inject.param('client', 'erply')
    def save_user(self, request, user, form, commit=True, client=None):
        data = form.cleaned_data

        # group_id = int(data.get('country'))
        # country = group2country(group_id)

        new_user = client.create_user(
            email=data.get('email'),
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
            password=data.get('password'),

            # groupID=group_id,
            companyName=data.get('company_name'),
            companyRegNr=data.get('company_reg_nr'),
            vatNumber=data.get('company_vat_nr')
        )

        user_email(user, new_user['email'])
        user_username(user, new_user['email'])
        user_field(user, 'first_name', new_user['firstName'])
        user_field(user, 'last_name', new_user['lastName'])

        # user.erply_customer_group = group_id
        # user.erply_customer_country = country.id
        # user.eu_resident = country.is_eu
        user.company_name = data.get('company_name')
        user.erply_customer_id = new_user['id']

        if 'password1' in data:
            user.set_password(data["password1"])
        else:
            user.set_unusable_password()

        self.populate_username(request, user)

        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user
