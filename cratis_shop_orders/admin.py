from cratis_shop_orders.models import OrderItem, Order
from django.contrib import admin

class InlineOrderItems(admin.TabularInline):
    model = OrderItem
    extra = 0
    # fields = ('product', 'quantity', 'unit_price')
    # readonly_fields = ('quantity', 'unit_price', 'product',)



# def generate_invoice(modeladmin, request, queryset):
#     for obj in queryset:
#         add_invoice_delivery_task(obj)
#
# generate_invoice.short_description = "Generate broken invoice"

class OrderAdmin(admin.ModelAdmin):

    # list_display = ('date_created', 'email', 'full_price', 'status', 'payment_method', 'documents', 'ip_address')
    inlines = [InlineOrderItems]
    # readonly_fields = ('delivery_method', 'address', 'payment_method', 'ip_address',)
    # exclude = ('smartpost_box','payment_status', 'phone', 'payment_response', 'smartpost_box_name')
    # list_filter = ('status', 'ip_address',)
    ordering = ('-date_created',)
    # actions = [generate_invoice]

admin.site.register(Order, OrderAdmin)






