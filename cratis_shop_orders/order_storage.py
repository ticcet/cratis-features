from decimal import Decimal
from cratis_shop_orders.models import Order, OrderItem


class LocalOrderStorage(object):

    def convert_order(self, order):
        o = {
            'id': order.id,
            'rows': [],
            'total': order.payment.sum,
            'state': 'paid' if order.is_paid else 'unpaid',
            'number': order.order_nr,
            'currency': order.currencyCode,
            'currency_rate': order.currencyRate,
        }

        for item in order.items.all():
            o['rows'].append({
                'productID': item.product_id,
                'itemName': item.product_name,
                'amount': item.product_qty,
                'finalPriceWithVAT': item.product_price,
                'rowTotal': item.product_price * item.product_qty,
            })

        return o


    def get_user_orders(self, user):
        orders = {
            'all': [],
            'unpaid': [],
            'paid': [],
            'sent': [],
        }

        for order in Order.objects.filter(user=user):
            data = self.convert_order(order)
            if order.is_paid:
                orders['paid'].append(data)
            else:
                orders['unpaid'].append(data)

            orders['all'].append(data)


        return orders

    def send_order(self):
        pass

    def payment_success(self, order, amount, po):
        po.order.is_paid = True
        po.order.save()

    def confirm_order(self, region, order, po, paid):

        try:
            o = po.order
        except Order.DoesNotExist:
            o = Order()
        o.payment = po
        o.currencyCode = order['currency']['code']
        o.currencyRate = order['currency']['rate']
        o.address = order['address']
        o.order_nr = str(100000 + po.pk)
        o.user = po.user
        o.is_paid = paid
        o.save()

        total = 0.0
        for i, item in enumerate(order['items'].values()):
            oi = OrderItem()
            oi.product_id = item['productID']
            oi.product_qty = item['qty']
            oi.product_price = item['price'] / float(order['currency']['rate'])
            oi.order = o
            oi.save()

            total += oi.product_price

        oi = OrderItem()
        oi.product_id = 0
        oi.product_qty = 1
        oi.product_price = order['totalDelivery'] / float(order['currency']['rate'])
        oi.order = o
        oi.save()

        po.document_id = o.order_nr
        po.no_tax = "%01.2f" % total
        po.tax = "%01.2f" % (float(total) * float(region.tax_rate) / 100)
        po.tax_rate = "%01.2f" % region.tax_rate
        po.sum = "%01.2f" % (float(total) + float(total) * float(region.tax_rate) / 100)


