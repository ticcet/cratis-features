# -*- coding: utf-8 -*-

from cratis_shop_payment.models import PaymentOrder
from django.conf import settings
from django.db import models
from django.db.models.deletion import SET_NULL
from django.db.models.fields import DateTimeField
from django.utils.translation import ugettext_lazy as _


class Order(models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    order_nr = models.CharField(max_length=55)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='orders', null=True, blank=True, on_delete=SET_NULL)
    is_sent = models.BooleanField(default=True, verbose_name=_('Invoice has been delivered'))
    comment = models.TextField(blank=True, null=True)

    is_paid = models.BooleanField(default=False)

    payment = models.OneToOneField(PaymentOrder, null=True, blank=True, related_name='order')

    currencyCode = models.CharField(max_length=10, default='EUR')
    currencyRate = models.DecimalField(decimal_places=8, max_digits=12, default=1.0)

    # def documents(self):
    #     return 'Invoice: <a href="%s" target="_blank"><img style="margin-bottom: -8px;" src="/assets/img/pdf_icon.gif" alt=""></a>' % self.invoice_pdf_url() + \
    #     ' Shipping list: <a href="%s" target="_blank"><img style="margin-bottom: -8px;" src="/assets/img/pdf_icon.gif" alt=""></a>' % self.shipping_list_pdf_url()
    # documents.allow_tags = True
    #
    # def shipping_list_pdf_url_href(self):
    #     return '<a href="%s" target="_blank"><img style="margin-bottom: -8px;" src="/assets/img/pdf_icon.gif" alt=""></a>' % self.shipping_list_pdf_url()
    # shipping_list_pdf_url_href.allow_tags = True
    #
    # def invoice_pdf_url_href(self):
    #     return '<a href="%s" target="_blank"><img style="margin-bottom: -8px;" src="/assets/img/pdf_icon.gif" alt=""></a>' % self.invoice_pdf_url()
    # invoice_pdf_url_href.allow_tags = True
    #
    # def invoice_pdf_url(self):
    #     return 'https://s3.amazonaws.com/maksa-shops-invoice/invoice-%s-%s.pdf' % (
    #         hashlib.sha256(str(self.pk) + "hoho12").hexdigest(),
    #         hashlib.sha256(str(self.pk) + "hoho21").hexdigest(),
    #     )
    # def shipping_list_pdf_url(self):
    #     return 'https://s3.amazonaws.com/maksa-shops-invoice/shipping-list-%s-%s.pdf' % (
    #         hashlib.sha256(str(self.pk) + "hoho12").hexdigest(),
    #         hashlib.sha256(str(self.pk) + "hoho21").hexdigest(),
    #     )

    def is_empty(self):
        return len(self.items.all) > 0

    def __unicode__(self):
        return "Order %s" % self.id

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')



class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items')

    product_id = models.CharField(max_length=55)
    product_name = models.CharField(max_length=255)
    product_qty = models.IntegerField()
    product_price = models.DecimalField(max_digits=10, decimal_places=5)
