from cratis.features import Feature
from django.conf.urls import patterns, include


class CratisAuction(Feature):

    def configure_settings(self):
        self.append_apps(['cratis_shop_auction',])

    def configure_urls(self, urls):
        urls += patterns('',
             (r'^', include('cratis_shop_auction.urls')),
        )