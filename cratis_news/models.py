from cratis_i18n.translate import TranslatableModelMixin
from django.db import models
from django.db.models.fields import DateTimeField

from django.utils.translation import ugettext_lazy as _
from filer.fields.image import FilerImageField


class Article(models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    title = models.CharField(max_length=255)
    text_short = models.TextField()
    text_long = models.TextField()

    image = FilerImageField(null=True, blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('News article')
        verbose_name_plural = _('News articles')
