from django.conf.urls import patterns
from cratis.features import Feature


class News(Feature):
    def configure_settings(self):
        self.append_apps([
            "cratis_news"
        ])