from django.conf.urls import patterns
from cratis.features import Feature


class SimpleBanners(Feature):
    def configure_settings(self):
        super(SimpleBanners, self).configure_settings()

        self.append_apps([
            "cratis_ads"
        ])

        self.append_template_processor(('cratis_ads.context.ads_context',))