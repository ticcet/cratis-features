from cratis_ads.models import Banner


def ads_context(request):

    banners = {}

    for banner in Banner.objects.all():
        banners[banner.name] = banner

    context = {
        'banners': banners
    }

    return context