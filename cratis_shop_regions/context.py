from cratis_rules.models import Rule
from cratis_shop.shop.frontend import CratisShopFrontend
from cratis_shop_regions.models import Language, Region
from django.utils.translation import get_language



def get_region(request):
    all_regions = Region.objects.all()
    if not len(all_regions):
        return None
    return all_regions[0]

def region_context(request):
    """
    @type context: HttpRequest
    """

    frontend = inject.instance(CratisShopFrontend)

    if request.path[0:7] == '/admin/':
        return {}

    try:
        current_lang = Language.objects.get(locale_code=get_language())
    except Language.DoesNotExist:
        current_lang = None


    try:
        region = get_region(request)

        # currency_info = frontend.get_currency_list()

        if region:
            cdata = dict([(x.currency, {'code': x.currency, 'name': x.name_on_site, 'symbol': x.symbol_on_site,
                                    'rate': 1 #currency_info[x.currency]['rate']
                                    }
                          ) for x in region.currencies])
            cdata['DEFAULT'] = cdata[cdata.keys()[0]]
            region_currencies = cdata


            region_countries = dict([(x.country.code, {
                    'name': unicode(x.country.name),
                    'code': unicode(x.country.code),
                    'row_delivery': x.get_row_rule(),
                    'total_delivery': x.get_total_rule(),
                    'express_delivery_index': x.express_delivery_index,
                    'free_delivery_at': x.free_delivery_at,
                    'weight_ranges': [
                        {
                            'from': r.weight_from,
                            'to': r.weight_to,
                            'value': r.value
                        } for r in x.delivery_weights.all()
                    ]

                }) for x in region.countries.filter(enabled=True)])

            region_langs = region.languages.all()
            region_tax = region.tax_rate

        else:
            region_currencies = []
            region_countries = []
            region_langs = []
            region_tax = None

        context = {
            'region': {
                'currencies': region_currencies,
                'countries': region_countries,
                # 'languages': region_langs,
                'tax_rate': region_tax,
                # 'lang': current_lang
            },
            # 'region_page': region.root_page if region else None
        }

        return context

    except Rule.DoesNotExist:
        return {
            'region': {
                'currencies': [],
                'countries': [],
                'languages': [current_lang],
                'tax_rate': 0,
                'lang': current_lang
            },
            'region_page': None
        }

