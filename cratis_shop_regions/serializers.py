import pickle
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.processors import colorspace, scale_and_crop, background
from rest_framework import serializers
from rest_framework.pagination import PaginationSerializer
from cratis_shop_payment.models import PaymentMethod
from cratis_shop_regions.models import Region, CurrencyPayemntMethod, Currency
from ticcet.models import Category, Place, User, PlaceEvent, OpenTime, Ticket, ThumbnailOption, \
    PlaceImage, TicketVariation, TicketDiscount, City, UserTicket, Contract
from django.utils import timezone
import json

class TiccetImageField(serializers.Field):

    def __init__(self, type):
        self.type = type

        super(TiccetImageField, self).__init__()

    def to_native(self, value):
        return super(TiccetImageField, self).to_native(value)


    def field_to_native(self, obj, field_name):
        data = {}
        picture = getattr(obj, field_name)
        if not picture:
            return None
        thumbnailer = get_thumbnailer(picture.image)
        thumbnailer.thumbnail_processors = [colorspace, scale_and_crop, background]

        request = self.context['request']
        if 'device' in request.QUERY_PARAMS:
            sizes = ThumbnailOption.objects.filter(devices__token=request.QUERY_PARAMS['device'], type__in=['all', self.type])
        else:
            sizes = ThumbnailOption.objects.filter(device=None, type__in=['all', self.type])

        for size in sizes:
            size_def = size.as_dict()
            thumb = thumbnailer.get_thumbnail(size_def)
            data[size.name] = {
                'url': thumb.url,
                'width': size_def['width'],
                'height': size_def['height']
            }
        return data


def serialaize_money(field):
    if field is None:
        return "0"
    field = float(field)
    if field.is_integer():
        return str(int(field))
    return "%0.2f" % field


class MoneyField(serializers.Field):
    def field_to_native(self, obj, field_name):
        field = getattr(obj, field_name)

        return serialaize_money(field)


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        # fields = ('id', 'name', 'enabled')
        exclude = (
            'name_en',
            'name_ru',
            'name_et',
        )

    image = TiccetImageField('category')

class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        exclude = (
            'name_en',
            'name_ru',
            'name_et',
        )
class UserContractSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contract
        fields = ('company',)

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'subscribe', 'password', 'token')
        write_only_fields = ('password',)
        depth = 2

    def restore_object(self, attrs, instance=None):
        obj = super(UserSerializer, self).restore_object(attrs, instance)

        if 'email' in attrs:
            obj.username = attrs['email']
        if 'password' in attrs:
            obj.set_password(attrs['password'])

        return obj

class EventSerializer(serializers.ModelSerializer):
    name = serializers.CharField()

    class Meta:
        model = PlaceEvent
        fields = ('id', 'name')
        exclude = ('contract',)


class PlaceGallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceImage

    image = TiccetImageField('gallery')

    def field_to_native(self, obj, field_name):
        result = super(PlaceGallerySerializer, self).field_to_native(obj, field_name)

        return [x for x in result if not x['image'] is None]


class OpenTimeSerializer(serializers.ModelSerializer):

    class Meta:
        model = OpenTime
        fields = ('description', 'order', 'is_open', 'name', 'time_from', 'time_to')

    description = serializers.CharField()


class PlaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Place
        depth = 0
        exclude = (
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
            'contract',
        )

    image = TiccetImageField('place')
    position = serializers.CharField()

    pictures = PlaceGallerySerializer(many=True)

    address = serializers.CharField()

    normal_price = MoneyField()
    ticcet_price = MoneyField()

    working_hours = serializers.SerializerMethodField('get_hours')

    def get_hours(self, obj):
        instance = OpenTime.objects.filter(place=obj, is_open=True)
        return OpenTimeSerializer(instance=instance, many=True, context=self.context).data



class PlaceEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = PlaceEvent
        depth = 1
        exclude = (
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
            'contract',
        )

    name = serializers.CharField()
    image = TiccetImageField('place')
    places = PlaceSerializer(many=True)

    pictures = PlaceGallerySerializer(many=True)
    categories = CategorySerializer(many=True)

    is_open_now = serializers.BooleanField()


    normal_price = MoneyField()
    ticcet_price = MoneyField()

    working_hours = serializers.SerializerMethodField('get_hours')

    def get_hours(self, obj):
        instance = OpenTime.objects.filter(event=obj, is_open=True)
        return OpenTimeSerializer(instance=instance, many=True, context=self.context).data

class TicketVariationSerializer(serializers.ModelSerializer):

    class Meta:
        model = TicketVariation

        exclude = (
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
        )


    price = MoneyField()



class TicketDiscountSerializer(serializers.ModelSerializer):

    class Meta:
        model = TicketDiscount
        exclude = (
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
        )


class TicketSerializer(serializers.ModelSerializer):

    class Meta:
        model = Ticket

        exclude = (
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
        )


    is_active_now = serializers.BooleanField()

    variations = TicketVariationSerializer(many=True)
    discounts = TicketDiscountSerializer(many=True)

    image = TiccetImageField('ticket')



class PlaceSearchSerializer(serializers.ModelSerializer):



    class Meta:
        model = PlaceEvent
        depth = 1
        exclude = (
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
            'contract',

            'location_name_en',
            'location_name_ru',
            'location_name_et',
            'address_en',
            'address_ru',
            'address_et',
            'price_text_1_en',
            'price_text_1_ru',
            'price_text_1_et',
            'price_text_2_en'
            'price_text_2_ru'
            'price_text_2_et'
        )

    name = serializers.CharField()
    image = TiccetImageField('place')

    # tickets = TicketSerializer(many=True)

    pictures = PlaceGallerySerializer(many=True)

    categories = CategorySerializer(many=True)

    distance = serializers.CharField()
    location = serializers.CharField()

    is_open_now = serializers.BooleanField()
    is_event = serializers.BooleanField()

    normal_price = MoneyField()
    ticcet_price = MoneyField()

    tickets = serializers.SerializerMethodField('get_items')
    places = serializers.SerializerMethodField('get_places')

    def get_places(self, obj):

        if isinstance(obj, PlaceEvent):
            places = PlaceSerializer(obj.places.all(), many=True, context=self.context).data

            instance = OpenTime.objects.filter(event=obj, is_open=True)
            times = OpenTimeSerializer(instance=instance, many=True, context=self.context).data

            field = TiccetImageField('place')
            field.context = self.context
            image = field.field_to_native(obj, 'image')

            for place in places:
                place['working_hours'] = times
                place['image'] = image

            return places
        else:
            return PlaceSerializer(obj.places(), many=True, context=self.context).data

    def get_items(self, obj):
        if isinstance(obj, PlaceEvent):
            instance = Ticket.objects.filter(events=obj)
        else:
            instance = Ticket.objects.filter(places=obj)

        instance = instance.filter(active_from__lt=timezone.now(), active_to__gt=timezone.now(), enabled=True)

        return TicketSerializer(instance=instance, many=True, context=self.context).data

#


class PaginatedSearchItemsSerializer(PaginationSerializer):

    """
    Serializes page objects of results querysets.
    """
    start_index = serializers.SerializerMethodField('get_start_index')
    end_index = serializers.SerializerMethodField('get_end_index')
    num_pages = serializers.Field(source='paginator.num_pages')
    current_page = serializers.SerializerMethodField('get_curr_page')

    def attributes(self):
        return super(PaginatedSearchItemsSerializer, self).attributes()

    def save_object(self, obj, **kwargs):
        super(PaginatedSearchItemsSerializer, self).save_object(obj, **kwargs)


    class Meta:
        object_serializer_class = PlaceSearchSerializer

    def get_start_index(self, page):
        return page.start_index()

    def get_end_index(self, page):
        return page.end_index()

    def get_curr_page(self, page):
        return page.number




class MyTicketsDataSerializer(serializers.Serializer):
    def to_native(self, obj):
        return pickle.loads(obj.data)


class MyTicketsTicketSerializer(TicketSerializer):
    # places = PlaceSerializer(many=True)
    # events = PlaceEventSerializer(many=True)

    places_and_events = PlaceEventSerializer(many=True)

    class Meta:
        model = Ticket
        exclude = (
            'data',
            'description_en',
            'description_ru',
            'description_et',
            'short_description_en',
            'short_description_ru',
            'short_description_et',
            'name_en',
            'name_ru',
            'name_et',
            'contract',
            'variations'
        )

class MyTicketsVariationSerializer(TicketVariationSerializer):
    ticket = MyTicketsTicketSerializer()

class MyTicketsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserTicket
        # depth = 2

    ticket_variation = MyTicketsVariationSerializer()

    valid_until = serializers.DateTimeField()


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency


class PaymentMethodSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentMethod
        exclude = ('config',)

class CurrencyPayemntMethodSerializer(serializers.ModelSerializer):
    currency = CurrencySerializer()
    payment_methods = PaymentMethodSerializer(many=True)

    class Meta:
        model = CurrencyPayemntMethod


class RegionSerializer(serializers.ModelSerializer):
    payments = CurrencyPayemntMethodSerializer(many=True)

    class Meta:
        model = Region


