# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'CountryDeliveryWeightIndex'
        db.create_table(u'cratis_shop_regions_countrydeliveryweightindex', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('weight_from', self.gf('django.db.models.fields.FloatField')()),
            ('weight_to', self.gf('django.db.models.fields.FloatField')()),
            ('value', self.gf('django.db.models.fields.FloatField')()),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(related_name='delivery_weights', to=orm['cratis_shop_regions.Country'])),
        ))
        db.send_create_signal(u'cratis_shop_regions', ['CountryDeliveryWeightIndex'])


    def backwards(self, orm):
        # Deleting model 'CountryDeliveryWeightIndex'
        db.delete_table(u'cratis_shop_regions_countrydeliveryweightindex')


    models = {
        u'cratis_shop_regions.country': {
            'Meta': {'object_name': 'Country'},
            'country': ('django_countries.fields.CountryField', [], {'default': "''", 'max_length': '2'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cratis_shop_regions.countrydeliveryweightindex': {
            'Meta': {'object_name': 'CountryDeliveryWeightIndex'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'delivery_weights'", 'to': u"orm['cratis_shop_regions.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'value': ('django.db.models.fields.FloatField', [], {}),
            'weight_from': ('django.db.models.fields.FloatField', [], {}),
            'weight_to': ('django.db.models.fields.FloatField', [], {})
        },
        u'cratis_shop_regions.countryextrafield': {
            'Meta': {'object_name': 'CountryExtraField'},
            'country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra_fields'", 'to': u"orm['cratis_shop_regions.Country']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'cratis_shop_regions.currency': {
            'Meta': {'object_name': 'Currency'},
            'currency': ('cratis_shop_regions.models.CurrencyField', [], {'default': "''", 'max_length': '10'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'symbol_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'})
        },
        u'cratis_shop_regions.language': {
            'Meta': {'object_name': 'Language'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'locale_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price_list': ('cratis_shop_regions.models.PriceListField', [], {'default': "''", 'max_length': '255'})
        },
        u'cratis_shop_regions.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Country']", 'null': 'True', 'blank': 'True'}),
            'currencies': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Currency']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Language']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'priceLists': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.PriceList']", 'null': 'True', 'blank': 'True'}),
            'tax_rate': ('django.db.models.fields.DecimalField', [], {'default': 'False', 'max_digits': '7', 'decimal_places': '2'})
        },
        u'cratis_shop_regions.regionextrafield': {
            'Meta': {'object_name': 'RegionExtraField'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'extra_fields'", 'to': u"orm['cratis_shop_regions.Region']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['cratis_shop_regions']