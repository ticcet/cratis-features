# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PriceList'
        db.create_table(u'cratis_shop_regions_pricelist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('price_list', self.gf('cratis_shop_regions.models.PriceListField')(default='', max_length=10)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'cratis_shop_regions', ['PriceList'])

        # Adding model 'Language'
        db.create_table(u'cratis_shop_regions_language', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('code', self.gf('django.db.models.fields.CharField')(default='', max_length=10)),
            ('name_on_site', self.gf('django.db.models.fields.CharField')(default='', max_length=250)),
            ('locale_code', self.gf('django.db.models.fields.CharField')(default='', max_length=10, null=True, blank=True)),
        ))
        db.send_create_signal(u'cratis_shop_regions', ['Language'])

        # Adding model 'Region'
        db.create_table(u'cratis_shop_regions_region', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=250)),
            ('geoip_filter_query', self.gf('django.db.models.fields.TextField')(default='', null=True, blank=True)),
        ))
        db.send_create_signal(u'cratis_shop_regions', ['Region'])

        # Adding M2M table for field languages on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_languages')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('language', models.ForeignKey(orm[u'cratis_shop_regions.language'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'language_id'])

        # Adding M2M table for field priceLists on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_priceLists')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('pricelist', models.ForeignKey(orm[u'cratis_shop_regions.pricelist'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'pricelist_id'])


    def backwards(self, orm):
        # Deleting model 'PriceList'
        db.delete_table(u'cratis_shop_regions_pricelist')

        # Deleting model 'Language'
        db.delete_table(u'cratis_shop_regions_language')

        # Deleting model 'Region'
        db.delete_table(u'cratis_shop_regions_region')

        # Removing M2M table for field languages on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_languages'))

        # Removing M2M table for field priceLists on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_priceLists'))


    models = {
        u'cratis_shop_regions.language': {
            'Meta': {'object_name': 'Language'},
            'code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'locale_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'price_list': ('cratis_shop_regions.models.PriceListField', [], {'default': "''", 'max_length': '10'})
        },
        u'cratis_shop_regions.region': {
            'Meta': {'object_name': 'Region'},
            'geoip_filter_query': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Language']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'priceLists': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.PriceList']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['cratis_shop_regions']