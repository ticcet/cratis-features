# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Country'
        db.create_table(u'cratis_shop_regions_country', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('enabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'cratis_shop_regions', ['Country'])

        # Deleting field 'Region.geoip_filter_query'
        db.delete_column(u'cratis_shop_regions_region', 'geoip_filter_query')

        # Adding M2M table for field countries on 'Region'
        m2m_table_name = db.shorten_name(u'cratis_shop_regions_region_countries')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('region', models.ForeignKey(orm[u'cratis_shop_regions.region'], null=False)),
            ('country', models.ForeignKey(orm[u'cratis_shop_regions.country'], null=False))
        ))
        db.create_unique(m2m_table_name, ['region_id', 'country_id'])


    def backwards(self, orm):
        # Deleting model 'Country'
        db.delete_table(u'cratis_shop_regions_country')

        # Adding field 'Region.geoip_filter_query'
        db.add_column(u'cratis_shop_regions_region', 'geoip_filter_query',
                      self.gf('django.db.models.fields.TextField')(default='', null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field countries on 'Region'
        db.delete_table(db.shorten_name(u'cratis_shop_regions_region_countries'))


    models = {
        u'cratis_shop_regions.country': {
            'Meta': {'object_name': 'Country'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'cratis_shop_regions.currency': {
            'Meta': {'object_name': 'Currency'},
            'currency': ('cratis_shop_regions.models.CurrencyField', [], {'default': "''", 'max_length': '10'}),
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'symbol_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'})
        },
        u'cratis_shop_regions.language': {
            'Meta': {'object_name': 'Language'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'locale_code': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '10'}),
            'name_on_site': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'})
        },
        u'cratis_shop_regions.pricelist': {
            'Meta': {'object_name': 'PriceList'},
            'enabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_main': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price_list': ('cratis_shop_regions.models.PriceListField', [], {'default': "''", 'max_length': '255'})
        },
        u'cratis_shop_regions.region': {
            'Meta': {'object_name': 'Region'},
            'countries': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Country']", 'null': 'True', 'blank': 'True'}),
            'currencies': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Currency']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'languages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.Language']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '250'}),
            'priceLists': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['cratis_shop_regions.PriceList']", 'null': 'True', 'blank': 'True'}),
            'tax_rate': ('django.db.models.fields.DecimalField', [], {'default': 'False', 'max_digits': '7', 'decimal_places': '2'})
        }
    }

    complete_apps = ['cratis_shop_regions']