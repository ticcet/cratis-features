# coding: utf-8
import os
from voluptuous import Schema, MultipleInvalid
from cratis.features import Feature


class I18n(Feature):

    def __init__(self, langs=(('en', 'English'),), main_lang='en', admin_lang=None, fallback_translations=False):
        self.main_lang = main_lang
        self.admin_lang = admin_lang if admin_lang else main_lang
        self.langs = langs

        self.fallback_translations = fallback_translations

        try:
            Schema(((str, basestring),))(langs)
        except MultipleInvalid as e:
            self.report_failure('Parameter "langs" validation error: %s' % str(e))

    def configure_settings(self):

        self.append_apps([
                'cratis_i18n',
                'modeltranslation',
            ])

        cls = self.settings

        cls.USE_FALLBACK_TRANSLATION = self.fallback_translations
        cls.LANGUAGE_CODE = self.main_lang
        cls.USE_I18N = True
        cls.USE_L10N = True

        if self.langs:
            cls.LANGUAGES = self.langs

        cls.MAIN_LANGUAGE = self.main_lang
        cls.ADMIN_LANGUAGE = self.admin_lang


        self.settings.TEMPLATE_DIRS += (os.path.dirname(__file__) + '/templates',)

        self.append_middleware(('cratis_i18n.middleware.LocaleRewriteMiddleware',))

        self.append_template_processor(('cratis_i18n.context.i18n_context',))

        cls.USE_FALLBACK_TRANSLATION = self.fallback_translations


class I18nAdminUi(Feature):

    def get_required_packages(self):
        return 'django-rosetta',

    def configure_settings(self):
        # self.append_apps([
        #     'rosetta',
        # ])
        #
        cls = self.settings

        # cls.ROSETTA_WSGI_AUTO_RELOAD = True
        # cls.ROSETTA_MESSAGES_PER_PAGE = 20
        # cls.ROSETTA_STORAGE_CLASS = 'rosetta.storage.SessionRosettaStorage'


    def configure_urls(self, urls):
        pass
        # from django.conf.urls import url, include, patterns

        # urls += patterns('',
        #     url(r'^admin/i18n/', include('rosetta.urls')),
        # )



