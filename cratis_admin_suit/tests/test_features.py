from cratis.settings import CratisConfig
from cratis_admin_suit.features import AdminThemeSuit
from flexmock import flexmock


def test_admin_feature():

    settings = flexmock(INSTALLED_APPS=(), TEMPLATE_CONTEXT_PROCESSORS=())

    feature = AdminThemeSuit(title='Foo')
    feature.set_settings(settings)

    feature.configure_settings()

    assert settings.INSTALLED_APPS == ('suit',)

    assert settings.SUIT_CONFIG == {
        'ADMIN_NAME': 'Foo'
    }
    assert settings.TEMPLATE_CONTEXT_PROCESSORS == ('django.core.context_processors.request',)

