package maksa.shops.reporter;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

import maksa.shops.reporter.domain.Invoice;
import maksa.shops.reporter.domain.InvoiceRow;
import net.sf.dynamicreports.report.builder.DynamicReports;
import net.sf.dynamicreports.report.builder.barcode.Code128BarcodeBuilder;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.component.VerticalListBuilder;
import net.sf.dynamicreports.report.builder.datatype.BigDecimalType;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.constant.VerticalAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

/**
 *
 */
public class ReportService {

    public void buildShippingList(Invoice invoice, OutputStream outputStream) {
        try {
//            FileOutputStream stream = new FileOutputStream(
//                    "invoice-" + id + ".pdf"
//            );

            try {
                StyleBuilder boldStyle         = stl.style().bold().setFontSize(25);
                StyleBuilder boldCenteredStyle = stl.style(boldStyle)
                        .setHorizontalAlignment(HorizontalAlignment.CENTER);
                StyleBuilder centeredStyle = stl.style()
                        .setHorizontalAlignment(HorizontalAlignment.CENTER);

                StyleBuilder columnTitleStyle  = stl.style(centeredStyle)
                        .setBorder(stl.pen1Point().setLineColor(Color.WHITE))
                        .setBackgroundColor(Color.LIGHT_GRAY);


                StyleBuilder titleStyle        = stl.style(boldCenteredStyle)
                        .setVerticalAlignment(VerticalAlignment.TOP)
                        .setForegroundColor(Color.LIGHT_GRAY)
                        .setFontSize(18);

                StyleBuilder normalRight   = stl.style().setFontSize(25)
                        .setHorizontalAlignment(HorizontalAlignment.RIGHT);

                StyleBuilder boldRight   = stl.style(normalRight)
                        .bold();

                StyleBuilder addressStyle        = stl.style()
                        .setVerticalAlignment(VerticalAlignment.MIDDLE)
                        .setLeftPadding(20)
                        .setFontSize(25);

                //Code128BarcodeBuilder shipToPostalCode = bcode.code128(invoice.getId().toString())
                  //      .setModuleWidth(2d);

                report()//create new report design

                        .setPageMargin(DynamicReports.margin(20))

                        .setColumnTitleStyle(columnTitleStyle)
                        .setSubtotalStyle(boldStyle)

                        .title(//shows report title
                                cmp.verticalList()
                                        .add(
                                                cmp.horizontalList().add(
                                                        cmp.image(new ByteArrayInputStream(invoice.getLogo())).setFixedDimension(160, 160)
                                                                .setHorizontalAlignment(HorizontalAlignment.CENTER),
                                                        cmp.horizontalGap(190),
                                                        cmp.verticalList().add(
                                                                cmp.verticalGap(25),
                                                                //shipToPostalCode,
                                                                cmp.verticalGap(65)
                                                        )

                                                ),
                                                cmp.text(invoice.t("sh_invoice_no") + invoice.getId()).setStyle(boldRight).setHorizontalAlignment(HorizontalAlignment.CENTER),
                                                cmp.verticalGap(20),
                                                cmp.verticalList().add(
                                                        cmp.text(invoice.t("sh_invoice_from"))
                                                                .setStyle(stl.style(addressStyle)
                                                                        .setBackgroundColor(Color.LIGHT_GRAY)
                                                                        .setLeftPadding(5)
                                                                )
                                                                .setHorizontalAlignment(HorizontalAlignment.LEFT),
                                                        cmp.text(invoice.getShippingReturnAddress())
                                                                .setStyle(stl.style(addressStyle).setTopPadding(5))
                                                                .setHorizontalAlignment(HorizontalAlignment.LEFT)
                                                ),
                                                cmp.verticalGap(20),
                                                cmp.verticalList().add(
                                                        cmp.text(invoice.t("sh_invoice_to"))
                                                                .setStyle(stl.style(addressStyle).setBackgroundColor(Color.LIGHT_GRAY)
                                                                        .setLeftPadding(5))
                                                                .setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                                        cmp.text(invoice.getShippingAddress())
                                                                .setStyle(stl.style(addressStyle).setTopPadding(5))
                                                                .setHorizontalAlignment(HorizontalAlignment.RIGHT)
                                                )
                                        )
                        )

                        .toPdf(outputStream);
//                        .show();

            } catch (DRException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void build(Invoice invoice, OutputStream outputStream) {
        try {

            try {
                StyleBuilder boldStyle         = stl.style().bold();
                StyleBuilder boldCenteredStyle = stl.style(boldStyle)
                        .setHorizontalAlignment(HorizontalAlignment.CENTER);
                StyleBuilder centeredStyle = stl.style()
                        .setHorizontalAlignment(HorizontalAlignment.CENTER);

                StyleBuilder columnTitleStyle  = stl.style(centeredStyle)
                        .setBorder(stl.pen1Point().setLineColor(Color.WHITE))
                        .setBackgroundColor(Color.LIGHT_GRAY);


                StyleBuilder titleStyle        = stl.style(boldCenteredStyle)
                        .setVerticalAlignment(VerticalAlignment.TOP)
                        .setForegroundColor(Color.GRAY)
                        .setFontSize(18);

                StyleBuilder normalRight   = stl.style()
                        .setHorizontalAlignment(HorizontalAlignment.RIGHT);

                StyleBuilder boldRight   = stl.style(normalRight)
                        .bold();

                StyleBuilder addressStyle        = stl.style()
                        .setVerticalAlignment(VerticalAlignment.MIDDLE)
                        .setLeftPadding(20)
                        .setFontSize(10);

                CurrencyType currencyType = new CurrencyType(invoice.t("$"));


                // No. column
                TextColumnBuilder<Integer> rowNumberColumn =
                        col.reportRowNumberColumn(invoice.t("invoice_row_no_label"))
                                //sets the fixed width of a column, width = 2 * character width
                                .setFixedColumns(2)
                                .setHorizontalAlignment(HorizontalAlignment.CENTER);

                TextColumnBuilder<Integer> idColumn = col.column(invoice.t("invoice_col_id"), "id", type.integerType())
                        .setFixedColumns(6).setHorizontalAlignment(HorizontalAlignment.LEFT);
                TextColumnBuilder<String> descriptionColumn = col.column(invoice.t("invoice_col_description"), "title", type.stringType())
                        .setWidth(40);
                TextColumnBuilder<BigDecimal> deliveryColumn = col.column(invoice.t("invoice_col_delivery"), "delivery", currencyType)
                        .setWidth(10);
                TextColumnBuilder<BigDecimal> unitPriceColumn = col.column(invoice.t("invoice_col_unit_price"), "unit", currencyType)
                        .setWidth(15);
                TextColumnBuilder<Integer> quantityColumn = col.column(invoice.t("invoice_col_quantity"), "qty", type.integerType())
                        .setFixedColumns(4);
                TextColumnBuilder<BigDecimal> totalColumn = col.column(invoice.t("invoice_col_total"), "total", currencyType)
                        .setWidth(15);

                DecimalFormat decimalFormat = new DecimalFormat();
                decimalFormat.setMinimumFractionDigits(2);

                VerticalListBuilder noteList = cmp.verticalList();
                if (invoice.getNotes().size() > 0) {
                    noteList.add(
                            cmp.text(invoice.t("invoice_notes_label"))
                    );
                    for(String note: invoice.getNotes()) {
                        noteList.add(cmp.text(note));
                    }
                }

                report()//create new report design
                        .setPageMargin(DynamicReports.margin(20))

                        .setColumnTitleStyle(columnTitleStyle)
                        .setSubtotalStyle(boldStyle)
                        .highlightDetailEvenRows()

                        .title(//shows report title
                                cmp.horizontalList()
                                        .add(
                                                cmp.image(new ByteArrayInputStream(invoice.getLogo())).setFixedDimension(120, 80),
                                                cmp.horizontalGap(45),
                                                cmp.verticalList()
                                                        .add(
                                                                cmp.text(invoice.getInfoHeader())
                                                                        .setStyle(stl.style(addressStyle).setBottomPadding(20).setVerticalAlignment(VerticalAlignment.TOP))
                                                                        .setHorizontalAlignment(HorizontalAlignment.LEFT)
                                                                        .setWidth(100)
                                                        ),
                                                cmp.verticalList()
                                                        .add(
                                                                cmp.text(invoice.t("invoice_label")).setStyle(titleStyle).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                                                cmp.horizontalList().add(
//                                                    cmp.horizontalGap(100),
                                                                        cmp.verticalList().add(
                                                                                cmp.text(invoice.t("invoice_date_label")).setStyle(stl.style(boldRight)).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                                                                cmp.text(invoice.t("invoice_no_label")).setStyle(stl.style(boldRight)).setHorizontalAlignment(HorizontalAlignment.RIGHT)
                                                                        ),
                                                                        cmp.verticalList().add(
                                                                                cmp.text(invoice.getDate()).setStyle(normalRight),
                                                                                cmp.text(invoice.getId()).setStyle(normalRight)
                                                                        ).setWidth(50)
                                                                ).setStyle(stl.style().setHorizontalAlignment(HorizontalAlignment.RIGHT))


                                                        )
                                        )
                                        .newRow()
                                        .add(cmp.filler().setStyle(stl.style()
                                                .setTopBorder(stl.pen2Point().setLineWidth(1.5f).setLineColor(Color.LIGHT_GRAY))).setFixedHeight(10))
                                        .newRow()
                                        .add(cmp.verticalGap(10))
                                        .newRow()
                                        .add(
                                                cmp.horizontalList().add(
                                                        shipAddress(invoice.t("invoice_bill_to"), invoice.getBillAddress()),
                                                        cmp.horizontalGap(30),
                                                        shipAddress(invoice.t("invoice_ship_to"), invoice.getShippingAddress()),
                                                        cmp.horizontalGap(100)
                                                )
                                        )
                                        .newRow()
                                        .add(cmp.verticalGap(30))
                        )



                        .pageFooter(cmp.pageXofY().setStyle(boldCenteredStyle))

                        .columns(
                                rowNumberColumn,
                                idColumn,
                                descriptionColumn,
                                deliveryColumn,
                                unitPriceColumn,
                                quantityColumn,
                                totalColumn
                        )

                        .subtotalsAtSummary(
                                sbt.sum(deliveryColumn)
                                        .setLabelStyle(boldStyle),
                                sbt.sum(totalColumn)
                        )
                        .summary(
                                cmp.horizontalList().add(
                                        cmp.horizontalGap(400),
                                        cmp.verticalList()
                                                .add(
                                                        cmp.verticalGap(30),
                                                        cmp.horizontalList().add(
                                                                cmp.verticalList().add(
                                                                        cmp.text(invoice.t("invoice_subtotal")).setStyle(boldRight).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                                                        cmp.text(invoice.t("invoice_tax")).setStyle(boldRight).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                                                        cmp.text(invoice.t("invoice_tax_total")).setStyle(boldRight).setHorizontalAlignment(HorizontalAlignment.RIGHT),
                                                                        cmp.text(invoice.t("invoice_total")).setStyle(stl.style(boldRight).setTopPadding(10)).setHorizontalAlignment(HorizontalAlignment.RIGHT)
                                                                ),
                                                                cmp.verticalList().add(
                                                                        cmp.text(decimalFormat.format(invoice.getSubtotal()) + " " + invoice.t("$")).setStyle(normalRight),
                                                                        cmp.text(decimalFormat.format(invoice.getTax()) + "%").setStyle(normalRight),
                                                                        cmp.text(decimalFormat.format(invoice.getTaxTotal()) + " " + invoice.t("$")).setStyle(normalRight),
                                                                        cmp.text(decimalFormat.format(invoice.getTotal()) + " " + invoice.t("$")).setStyle(stl.style(boldRight).setTopPadding(10))
                                                                )
                                                        ).setStyle(stl.style().setHorizontalAlignment(HorizontalAlignment.RIGHT))
                                                )
                                ),
                                cmp.verticalGap(50),
                                noteList
                        )

                        .addLastPageFooter(cmp.horizontalList().setHeight(120)
                            .add(cmp.filler().setStyle(stl.style()
                                    .setTopBorder(stl.pen2Point().setLineWidth(1.5f).setLineColor(Color.GRAY))).setFixedHeight(10))
                            .newRow()
                            .add(
                                cmp.verticalList()
                                        .add(
                                                cmp.text(invoice.getInfoFooterCol1())
                                                        .setStyle(stl.style(addressStyle).setBottomPadding(20).setVerticalAlignment(VerticalAlignment.TOP))
                                                        .setHorizontalAlignment(HorizontalAlignment.LEFT)
                                                        .setWidth(150)
                                        ),
                                cmp.verticalList()
                                        .add(
                                                cmp.text(invoice.getInfoFooterCol2())
                                                        .setStyle(stl.style(addressStyle).setBottomPadding(20).setVerticalAlignment(VerticalAlignment.TOP))
                                                        .setHorizontalAlignment(HorizontalAlignment.LEFT)
                                                        .setWidth(150)
                                        ),
                                cmp.verticalList()
                                        .add(
                                                cmp.text(invoice.getInfoFooterCol3())
                                                        .setStyle(stl.style(addressStyle).setBottomPadding(20).setVerticalAlignment(VerticalAlignment.TOP))
                                                        .setHorizontalAlignment(HorizontalAlignment.LEFT)
                                                        .setWidth(150)
                                        )
                        ))

                        .setDataSource(createDataSource(invoice.getRows()))//set datasource
                        .toPdf(outputStream);//create and show report
//                        .show();

            } catch (DRException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private VerticalListBuilder shipAddress(String title, String address) {

        StyleBuilder addressStyle        = stl.style()
                .setVerticalAlignment(VerticalAlignment.MIDDLE)
                .setLeftPadding(5)
                .setFontSize(10);

        return cmp.verticalList().add(
                cmp.text(title)
                        .setStyle(stl.style(addressStyle).setBackgroundColor(Color.LIGHT_GRAY).setLeftPadding(5))
                        .setHorizontalAlignment(HorizontalAlignment.LEFT),
                cmp.text(address)
                        .setStyle(stl.style(addressStyle).setTopPadding(5))
                        .setHorizontalAlignment(HorizontalAlignment.LEFT)
        );
    }


    private class CurrencyType extends BigDecimalType {
        private static final long serialVersionUID = 1L;

        private String currencySymbol;

        private CurrencyType(String currencySymbol) {
            this.currencySymbol = currencySymbol;
        }

        @Override
        public String getPattern() {
            return "0.00 " + currencySymbol;
        }
    }

    private JRDataSource createDataSource(List<InvoiceRow> rows) {
        DRDataSource dataSource = new DRDataSource("id", "title", "qty", "unit", "delivery", "total");
        for(InvoiceRow row : rows) {
            dataSource.add(
                    row.getId(),
                    row.getTitle(),
                    row.getQty(),
                    row.getUnitPrice(),
                    row.getDeliveryPrice(),
                    row.getTotalPrice()
                );
        }
        
        return dataSource;
    }
}
