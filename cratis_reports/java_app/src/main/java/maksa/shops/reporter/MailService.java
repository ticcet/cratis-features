package maksa.shops.reporter;

import maksa.shops.reporter.domain.Invoice;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;


/**
 * @author Alex Rudakov
 */
public class MailService {

    public void sendMessages(Invoice invoice, byte[] invoicePdf, byte[] shippingListPdf) {
        
        // send client message
        
        Map<String, byte[]> attachements = new HashMap<String, byte[]>();
        attachements.put("invoice-" + invoice.getId() + ".pdf", invoicePdf);

        email(invoice.getFromEmail(), invoice.getEmail(), "Ivoice " + invoice.getId(),
                "Here is your invoice attached", attachements);

        attachements.put("shipping-list-" + invoice.getId() + ".pdf", shippingListPdf);
        email(invoice.getFromEmail(), invoice.getAdminEmail(), "Ivoice " + invoice.getId(),
                "New order for " + invoice.getTotal() + " SEK!", attachements);
    }

    /**
     * Sends an email with a PDF attachment.
     * @param sender Sender email
     * @param recipient Recipient email
     * @param subject Messae subject
     * @param content Content of message
     * @param attachements Attachements
     */
    public void email(String sender, String recipient, String subject,  String content, Map<String, byte[]> attachements) {

//        String smtpHost = "localhost"; //replace this with a valid host
//        int smtpPort = 25; //replace this with a valid port
//
        Properties properties = new Properties();
//        properties.put("mail.smtp.host", smtpHost);
//        properties.put("mail.smtp.port", smtpPort);
        try {
            Session session = Session.getDefaultInstance(properties, null);
            Transport tr = session.getTransport("smtp");
            tr.connect("email-smtp.us-east-1.amazonaws.com", "AKIAJOVVYA5MNG4QB37Q", "AugXsT383t0vWfyrFGkQKu0U9qR/IeV1B+IhvSipseWk");

            ByteArrayOutputStream outputStream = null;

            //construct the text body part
            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText(content);


            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(textBodyPart);

            for(String key : attachements.keySet()) {
                //construct the pdf body part
                DataSource dataSource = new ByteArrayDataSource(attachements.get(key), "application/pdf");
                MimeBodyPart pdfBodyPart = new MimeBodyPart();
                pdfBodyPart.setDataHandler(new DataHandler(dataSource));
                pdfBodyPart.setFileName(key);

                mimeMultipart.addBodyPart(pdfBodyPart);
            }

            //create the sender/recipient addresses
            InternetAddress iaSender = new InternetAddress(sender);
            InternetAddress iaRecipient = new InternetAddress(recipient);

            //construct the mime message
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setSender(iaSender);
            mimeMessage.setFrom(iaSender);
            mimeMessage.setSubject(subject);
            mimeMessage.setRecipient(Message.RecipientType.TO, iaRecipient);
            mimeMessage.setContent(mimeMultipart);

            //send off the email
            tr.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            tr.close();

            System.out.println("sent from " + sender +
                    ", to " + recipient);


        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }
}
