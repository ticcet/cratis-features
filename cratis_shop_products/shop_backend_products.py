import sys

from cratis_shop.shop.api import ShopBackend
from cratis_shop_products.models import Product, ProductCategory

from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.processors import background, scale_and_crop, colorspace


def convert_product(product, parents=None):
    """
    :param product:
    :type product: cratis_shop_products.models.Product
    :return:
    """

    data = {
        'id': product.pk,
        'productID': product.pk,
        'type': 'PRODUCT',
        'active': product.active,
        'name': product.title,
        'long_name': product.long_title,
        'slug': product.slug,
        'description': product.description,
        'long_description': product.long_description,
        'groupID': product.category_ids(),
        # 'priceWithVat': product.price,
        'hasVariations': False,
        'isVariation': False,
        'parameters': [],
        'related': [],
    }
    if not parents:
        parents = []

    parents.append(product.pk)

    for related_product in product.related.all():
        if not related_product.pk in parents:
            data['related'].append(convert_product(related_product, parents))

    print data
    main_cat = product.main_category()
    if main_cat:
        data['mainGroupID'] = main_cat.id
        data['mainGroupName'] = main_cat.name

    for attr in product.attributes.all():
        data['parameters'].append({
            'parameterID': unicode(attr.id),
            'parameterValue': unicode(attr.value),
            'parameterName': unicode(attr.type.name),
        })

    if product.price_discount:
        data['price'] = product.price_discount
        data['discount'] = product.discount
        data['main_price'] = product.price
    else:
        data['price'] = product.price
        data['discount'] = None
        data['main_price'] = None


    return data


def convert_category(category):
    """
    :param category:
    :type category: cratis_shop_products.models.ProductCategory
    :return:
    """

    data = {
        'id': category.pk,
        'productGroupID': category.pk,
        'name': category.name,
        'slug': category.slug,
        'subGroups': [
            convert_category(cat) for cat in category.get_children()
        ]
    }

    return data


def download_image(image, sizes):

    generated_sizes = {}
    for size_name, size in sizes.items():
        thumbnailer = get_thumbnailer(image)
        thumbnailer.thumbnail_processors = [colorspace, scale_and_crop, background]
        thumb = thumbnailer.get_thumbnail(size)
        generated_sizes[size_name] = thumb.url
        sys.stdout.write('.')
        sys.stdout.flush()

    return generated_sizes


class CratisShopBackendBackend(ShopBackend):

    def fetch_products(self, changed_since=None):
        for product in Product.objects.filter(archived=False, active=True):
            yield convert_product(product)

    # def fetch_product(self, changed_since=None, id=0):
    #     data = get_client().fetch_one('getProducts', {'productIDs': id, 'getParameters': 1})
    #     return ErplyProduct(data)

    def fetch_categories(self, changed_since=None):
        for category in ProductCategory.objects.filter(level=0):
            yield convert_category(category)


    def fetch_price_lists(self):
        # lists = get_client().fetch_many('getPriceLists', {}, 300)
        #
        # lists = [l for l in lists if l['name'].startswith('grandex_')]

        return []

    def fetch_currency_list(self):
        # list = get_client().fetch_many('getCurrencies', {}, 300)

        return [
            {
                'currencyID': 1,
                'code': 'EUR',
                'name': 'Euro',
                'rate': 1,
            },
            {
                'currencyID': 2,
                'code': 'SEK',
                'name': 'SEK',
                'rate': 1,
            },
        ]

    def fetch_product_variations(self, changed_since=None):
        return []

    def fetch_parameter(self, changed_since=None, id=0):
        return None

    def fetch_parameters(self, changed_since=None):
        return []


    def download_product_pictures(self, product, sizes):

        product = Product.objects.get(pk=product['productID'])

        all_pics = []

        for pic in product.pictures.all():
            generated_sizes = download_image(pic.image, sizes)
            all_pics.append({'sizes': generated_sizes, 'id': pic.pk})

        return all_pics


    def download_category_pictures(self, category, sizes):

        all_pics = []
        # if 'images' in category:
        #     for image in category['images']:
        #         generated_sizes = download_image('category', image['largeURL'], category['productGroupID'], image['pictureID'], sizes)
        #         all_pics.append({'sizes': generated_sizes, 'id': image['pictureID']})

        return all_pics