import json
from cratis_utils.models import generate_code
import django
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.fields import DateTimeField
from django import forms
from django.db.models.signals import post_save
from django.dispatch import Signal, receiver
from filer.fields.image import FilerImageField
from voluptuous import MultipleInvalid
import yaml

from .fields import YamlFancyField
from cratis_i18n.translate import TranslatableModelMixin, translate_model
from cratis_shop_payment.methods import get_payment_method_selection, get_payment_method
from django.utils.translation import ugettext_lazy as _


class PaymentLog(models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    module = models.CharField(max_length=50, null=True, blank=True, default='')
    user_id = models.CharField(max_length=50, null=True, blank=True, default='')
    user_ip = models.CharField(max_length=50, null=True, blank=True, default='')
    operation = models.CharField(max_length=255, null=True, blank=True, default='')
    url = models.CharField(max_length=255, null=True, blank=True, default='')
    request_get = models.TextField(blank=True, null=True, default='')
    request_post = models.TextField(blank=True, null=True, default='')
    data = models.TextField(blank=True, null=True, default='')

class PaymentMethodKey(models.Model):
    name = models.CharField(max_length=150, null=True)
    payment = models.ForeignKey('PaymentMethod', related_name='keys')
    body = models.TextField(blank=True, null=True, default='')

    class Meta:
        unique_together = (("name", "payment"),)


slug_validator = RegexValidator(regex='^[^\d/][^/]+$',
                                message=_('Slug should not start with digit and contain "/" character'))


order_checkout = Signal(providing_args=["payment_order"])
order_paid = Signal(providing_args=["payment_order"])


class PaymentOrder(models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)
    body = models.TextField(blank=True, null=True, default='')

    checked_out = models.BooleanField(default=False)
    paid = models.BooleanField(default=False)

    document_id = models.CharField(blank=True, null=True, default='', max_length=100)
    code = models.CharField(blank=True, null=True, default='', max_length=10)
    sum = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    user = models.ForeignKey(getattr(settings, 'AUTH_USER_MODEL', 'auth.User'), blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.code:
            generate_code(PaymentOrder, self, 10)

        try:
            self.sum = self.data['total']
        except KeyError:
            pass

        super(PaymentOrder, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return '%s - %s' % (self.code, self.user)

    @property
    def data(self):
        if hasattr(self, '_raw'):
            return self._raw

        self._raw = json.loads(self.body)
        return self._raw

    @property
    def total_paid(self):
        return sum([p.amount for p in self.payments.all()])

    def checkout(self):
        self.checked_out = True
        self.save()
        order_checkout.send(sender=self.__class__, payment_order=self)

    def pay(self):
        total = 0

        for payment in self.payments.all():
            total += payment.amount

        if total >= self.sum:
            self.paid = True
            self.save()

            order_paid.send(sender=self.__class__, payment_order=self)


class Payment(models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    order = models.ForeignKey(PaymentOrder, related_name='payments')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    payment_ref = models.CharField(max_length=255, blank=True, null=True)

    method = models.ForeignKey('PaymentMethod', null=True, blank=True)

    code = models.CharField(blank=True, null=True, default='', max_length=10)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.code:
            generate_code(Payment, self, 10)

        super(Payment, self).save(force_insert, force_update, using, update_fields)

    def __unicode__(self):
        return '%s - %s' % (self.code, self.method.name)


@receiver(post_save, sender=Payment)
def payment_created(instance=None, created=False, **kwargs):
    if created:
        instance.order.pay()



class PaymentMethod(TranslatableModelMixin, models.Model):
    method = models.CharField(max_length=20, choices=get_payment_method_selection(), null=True)
    enabled = models.BooleanField(default=False, verbose_name=_('Enabled'))

    slug = models.CharField(max_length=255, verbose_name=_('slug'), default='', blank=False, null=False,
                            validators=[slug_validator])

    logo = FilerImageField(null=True, blank=True, related_name='payment_method_logo')
    picture = FilerImageField(null=True, blank=True, related_name='payment_method_picture')
    name = models.CharField(max_length=150, null=True)
    description = models.TextField(blank=True, default='')

    sorting = models.PositiveIntegerField(default=0)

    config = YamlFancyField(blank=True, null=True)

    def clean(self):
        try:
            method = self.behavior()
        except yaml.MarkedYAMLError as e:
            raise ValidationError('Config yaml parse error: ' + str(e))
        except MultipleInvalid as e:
            raise ValidationError('Config is not valid: ' + str(e))
        if not method:
            raise ValidationError('Payment method is incorrect.')

    def behavior(self):
        if not self.method:
            return None
        return get_payment_method(self, self.parse_config(), self.get_keys())

    @property
    def info(self):
        return self.behavior()

    def parse_config(self):
        return yaml.load(self.config)

    def get_keys(self):
        return dict([(x.name, x.body) for x in self.keys.all()])

    def __unicode__(self):
        return self.name


    class Meta:
        ordering = ('sorting', )


class InvoicePayment(models.Model):
    date_created = DateTimeField(auto_now_add=True)
    date_updated = DateTimeField(auto_now=True)

    order = models.ForeignKey(PaymentOrder, related_name='invoices')
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    payment_ref = models.CharField(max_length=255, blank=True, null=True)

    payment = models.ForeignKey(Payment, blank=True, null=True)
    method = models.ForeignKey(PaymentMethod, blank=False, null=True, related_name='invoice_payments')

    user = models.ForeignKey(getattr(settings, 'AUTH_USER_MODEL', 'auth.User'), blank=True, null=True)


    def __unicode__(self):
        return '%s - %s via %s' % (self.order.code, self.amount, self.method.name)


@receiver(post_save, sender=InvoicePayment)
def invoice_created(instance=None, created=False, **kwargs):
    if created:
        if not instance.order.checked_out:
            instance.order.checkout()

        p = Payment()
        p.order = instance.order
        p.amount = instance.amount
        p.payment_ref = instance.payment_ref
        p.method = instance.method
        p.save()

        instance.payment = p
        instance.save()

