import json
from urllib import urlencode
from django.core.urlresolvers import reverse
from django import forms
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from paypal.pro.exceptions import PayPalFailure
from paypal.pro.fields import CountryField, CreditCardField, CreditCardExpiryField, CreditCardCVV2Field
from paypal.pro.forms import ConfirmForm
from cratis import settings
from cratis_shop_payment.models import PaymentMethod, PaymentOrder

from paypal.pro.helpers import SANDBOX_ENDPOINT, ENDPOINT, PayPalWPP, VERSION
from paypal.pro.views import PayPalPro, SANDBOX_EXPRESS_ENDPOINT, EXPRESS_ENDPOINT


class PayPalView(PayPalWPP):

    def __init__(self, request, params):
        """Required - USER / PWD / SIGNATURE / VERSION"""
        self.request = request
        if params['TEST']:
            self.endpoint = SANDBOX_ENDPOINT
        else:
            self.endpoint = ENDPOINT

        self.signature_values = {
            'USER': params['USER'],
            'PWD': params['PASSWORD'],
            'SIGNATURE': params['SIGNATURE'],
            'VERSION': params['VERSION'],
        }
        self.signature = urlencode(self.signature_values) + "&"


class PaymentForm(forms.Form):

    """Form used to process direct payments."""
    firstname = forms.CharField(255, label="First Name")
    lastname = forms.CharField(255, label="Last Name")
    street = forms.CharField(255, label="Street Address")
    city = forms.CharField(255, label="City")
    state = forms.CharField(255, label="State")
    countrycode = CountryField(label="Country", initial="US")
    zip = forms.CharField(32, label="Postal / Zip Code")
    acct = CreditCardField(label="Credit Card Number")
    expdate = CreditCardExpiryField(label="Expiration Date")
    cvv2 = CreditCardCVV2Field(label="Card Security Code")

    def process(self, request, item):
        """Process a PayPal direct payment."""
        wpp = PayPalView(request, params=PaymentForm.cc_config)
        params = self.cleaned_data
        params['creditcardtype'] = self.fields['acct'].card_type
        params['expdate'] = self.cleaned_data['expdate'].strftime("%m%Y")
        params['ipaddress'] = request.META.get("REMOTE_ADDR", "")
        params.update(item)

        try:
            # Create single payment:
            if 'billingperiod' not in params:
                nvp_obj = wpp.doDirectPayment(params)
            # Create recurring payment:
            else:
                nvp_obj = wpp.createRecurringPaymentsProfile(
                    params, direct=True)
        except PayPalFailure:
            return False
        return True


class PayPalProView(PayPalPro):

    def __init__(
        self, config=None, item=None, payment_form_cls=PaymentForm, payment_template="pro/payment.html",
        confirm_form_cls=ConfirmForm, confirm_template="pro/confirm.html", success_url="?success",
        fail_url=None, context=None, form_context_name="form"):

        config['VERSION'] = VERSION
        self.cc_config = config

        PaymentForm.cc_config = config

        super(
            PayPalProView, self).__init__(item, payment_form_cls, payment_template, confirm_form_cls,
                                          confirm_template, success_url, fail_url, context, form_context_name)

    def get_endpoint(self):
        if self.cc_config['TEST']:
            return SANDBOX_EXPRESS_ENDPOINT
        else:
            return EXPRESS_ENDPOINT

    def redirect_to_express(self):
        """
        First step of ExpressCheckout. Redirect the request to PayPal using the
        data returned from setExpressCheckout.
        """
        wpp = PayPalView(self.request, params=self.cc_config)

        try:
            nvp_obj = wpp.setExpressCheckout(self.item)
        except PayPalFailure as e:
            print e
            print self.errors['paypal']
            self.context['errors'] = self.errors['paypal']
            return self.render_payment_form()
        else:
            pp_params = dict(token=nvp_obj.token, AMT=self.item['amt'],
                             RETURNURL=self.item['returnurl'],
                             CANCELURL=self.item['cancelurl'])
            pp_url = self.get_endpoint() % urlencode(pp_params)
            return HttpResponseRedirect(pp_url)

    def render_confirm_form(self):
        """
        Second step of ExpressCheckout. Display an order confirmation form which
        contains hidden fields with the token / PayerID from PayPal.
        """
        initial = dict(token=self.request.GET[
                       'token'], PayerID=self.request.GET['PayerID'])
        self.context[self.form_context_name] = self.confirm_form_cls(
            initial=initial)
        return render_to_response(self.confirm_template, self.context, RequestContext(self.request))

    def validate_confirm_form(self):
        """
        Third and final step of ExpressCheckout. Request has pressed the confirmation but
        and we can send the final confirmation to PayPal using the data from the POST'ed form.
        """
        wpp = PayPalView(self.request, params=self.cc_config)
        pp_data = dict(token=self.request.POST[
                       'token'], payerid=self.request.POST['PayerID'])
        self.item.update(pp_data)

        # @@@ This check and call could be moved into PayPalWPP.
        try:
            if self.is_recurring():
                nvp_obj = wpp.createRecurringPaymentsProfile(self.item)
            else:
                nvp_obj = wpp.doExpressCheckoutPayment(self.item)
        except PayPalFailure:
            self.context['errors'] = self.errors['processing']
            return self.render_payment_form()
        else:
            return HttpResponseRedirect(self.success_url)


def paypal_view_data(request, method):

    payment = get_object_or_404(PaymentMethod, slug=method)
    config = payment.behavior().config

    po = PaymentOrder.objects.get(pk=request.session['order_id'])
    order = json.loads(po.body)

    #
    # else:
    #         document = {
    #             'type': 'ORDER',
    #             'currencyCode': order['currency']['code'],
    #             'currencyRate': order['currency']['rate'],
    #             'customerID': self.request.user.erply_customer_id,
    #             'addressID': order['address'],
    #             'confirmInvoice': 1,
    #             'paymentType': 'TRANSFER',
    #             'reserveGoods': 0,
    #             'sendByEmail': 1,
    #             'customNumber': 'GR-123126',
    #             'allowDuplicateNumber': 1,
    #             'invoiceState': 'READY',
    #             'paymentStatus': 'UNPAID'
    #         }
    #
    #     last_idx = 0
    #     for i, item in enumerate(order['items'].values()):
    #         idx = i + 1
    #         document['productID%s' % idx] = item['productID']
    #         document['amount%s' % idx] = item['qty']
    #         document['price%s' % idx] = item['price'] / float(order['currency']['rate'])
    #         last_idx = idx
    #
    #     idx = last_idx + 1
    #     document['productID%s' % idx] = 8682
    #     document['amount%s' % idx] = 1
    #     document['price%s' % idx] = order['totalDelivery'] / float(order['currency']['rate'])
    #
    #     # order_ = get_client().fetch_one('saveSalesDocument', document)

    success_url = reverse('cratis_profile__main')
    return_url = reverse('paypal_payment_confirm', kwargs={'method': method})
    cancel_url = reverse('shop_checkout')

    return_url = request.build_absolute_uri(return_url)
    cancel_url = request.build_absolute_uri(cancel_url)
    success_url = request.build_absolute_uri(success_url)

    item = {"amt": po.sum,  # amount to charge for item
            "inv": request.session['order_id'],  # unique tracking variable paypal
            # "cart": order.cart,
            "currencycode": order['currency']['code'].upper(),
            # "custom": "tracking",       # custom tracking variable for you
            # Express checkout cancel url
            "cancelurl": cancel_url,
            "returnurl": return_url
    }
    # Express checkout return url

    # last_idx = 0
    # for i, oitem in enumerate(order['items'].values()):
    #     idx = i + 1
    #
    #     item['L_NAME' + str(idx)] = oitem['name']
    #     item['L_AMT' + str(idx)] = str(round(oitem['price'] / float(order['currency']['rate']), 2))
    #     item['L_QTY' + str(idx)] = oitem['qty']
    #     last_idx = idx
    #
    # idx = last_idx + 1
    #
    # item['L_NAME' + str(idx)] = 'Delivery'
    # item['L_AMT' + str(idx)] = str(round(order['totalDelivery'] / float(order['currency']['rate']), 2))
    # item['L_QTY' + str(idx)] = 1


    print(config)
    print(item)





    # return PayPalPro(item=item,
    # success_url=reverse('cratis_shop_orders.views.confirm'))

    return PayPalProView(item=item, success_url=success_url, config=config)


@never_cache
def payment_start(request, method):
    ppp = paypal_view_data(request, method)
    return ppp(request)


@csrf_exempt
@never_cache
def payment_confirm(request, method):
    ppp = paypal_view_data(request, method)
    return ppp(request)

@csrf_exempt
@never_cache
def payment_success(request, method):


    return render_to_response('bobo')
