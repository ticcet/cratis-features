from django.core.urlresolvers import reverse
from voluptuous import Required, Schema
from cratis_shop_payment.common import Payment


class MaksekeskusPayment(Payment):

    actions_template = 'payment/maksekeskus_actions.html'

    def schema(self):
        return Schema({
            'id': str,
            'url': str,
            'secret': str
        }, required=True)

    def require_payment(self):
        return True

    def payment_url(self):
        return reverse('maksekeskus_payment_start', kwargs={'method': self.model.slug})

