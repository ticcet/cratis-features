from django.conf.urls import patterns, url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from cratis_shop_payment.voucher.views import VoucherStart


urlpatterns = patterns('cratis_shop_payment.voucher.views',
    url(r'^(?P<method>[^\\]+)/pay', never_cache(VoucherStart.as_view()), name='voucher_payment_start')
)



