from cratis_shop_payment.common import Payment
from cratis_shop_payment.dibs.payment import DibsPayment
from cratis_shop_payment.fake.payment import FakePayment
from cratis_shop_payment.pangalink.payment import PangalinkPayment
from cratis_shop_payment.paypal.payment import PaymentPaypal
from cratis_shop_payment.paymill.payment import PaymillPayment
from cratis_shop_payment.kardikeskus.payment import KardikeskusPayment
from cratis_shop_payment.maksekeskus.payment import MaksekeskusPayment
from cratis_shop_payment.paytrail.payment import PaytrailPayment
from cratis_shop_payment.voucher.payment import VoucherPayment

__author__ = 'Alex'

from django.utils.translation import ugettext as _


class UnknownDeliveryMethod(Exception):
    pass


def get_payment_methods():
    return {
        'kardikeskus': KardikeskusPayment,
        'maksekeskus': MaksekeskusPayment,
        'pangalink': PangalinkPayment,
        'paypal': PaymentPaypal,
        'paymill': PaymillPayment,
        'paytrail': PaytrailPayment,
        'dibs': DibsPayment,
        'invoice': Invoice,
        'voucher': VoucherPayment,
        'fake': FakePayment,
    }


def get_payment_method(model, config, keys):
    name = model.method
    methods = get_payment_methods()
    if name in methods:
        return methods[name](model, config, keys)
    else:
        raise UnknownDeliveryMethod()


def get_payment_method_selection():
    return (
        ('kardikeskus', _('Kardikeskus payment')),
        ('maksekeskus', _('Maksekeskus payment')),
        ('pangalink', _('Pangalink')),
        ('paytrail', _('Paytrail payment')),
        ('paypal', _('PayPal payment')),
        ('paymill', _('Paymill payment')),
        ('dibs', _('DIBS payment')),
        ('invoice', _('Invoice payment')),
        ('voucher', _('Voucher payment')),
        ('fake', _('Fake payment (for testing)')),
    )


class Invoice(Payment):
    actions_template = 'payment/invoice.html'


