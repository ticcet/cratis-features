from cratis_shop_payment.views import CheckoutView
from django.conf.urls import patterns, url, include

from cratis_i18n.utils import localize_url as _

from django.views.generic import TemplateView

urlpatterns = patterns('',

    url(r'^payment/done/', TemplateView.as_view(template_name='payment_done.html'), name='payment_done'),
    url(r'^payment/dibs/', include('cratis_shop_payment.dibs.urls')),
    url(r'^payment/fake/', include('cratis_shop_payment.fake.urls')),
    # url(r'^payment/paypal/', include('cratis_shop_payment.paypal.urls')),
    url(r'^payment/pangalink/', include('cratis_shop_payment.pangalink.urls')),
    url(r'^payment/paytrail/', include('cratis_shop_payment.paytrail.urls')),
    url(r'^payment/kk/', include('cratis_shop_payment.kardikeskus.urls')),
    url(r'^payment/maksekeskus/', include('cratis_shop_payment.maksekeskus.urls')),
    # url(r'^payment/voucher/', include('cratis_shop_payment.voucher.urls')),

    url(_(r'^checkout/$'), CheckoutView.as_view(), name='shop_checkout'),
)
