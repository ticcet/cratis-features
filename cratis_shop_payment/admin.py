from cratis_admin.utils import DefaultFilterMixIn
from django.contrib import admin
from django.forms import TextInput, Textarea
from django.template import Template, Context

from suit.admin import SortableModelAdmin
from .fields import default_overrides
from cratis_shop_payment.models import PaymentMethod, PaymentMethodKey, InvoicePayment, Payment, \
    PaymentOrder
from django.db import models

# class PaymentLogAdmin(admin.ModelAdmin):
#     ordering = ('-id',)
#     list_display = ('module', 'operation', 'url', 'user_ip', 'user_id', 'date_created')
#
# admin.site.register(PaymentLog, PaymentLogAdmin)


class InlinePaymentMethodKey(admin.StackedInline):
    model = PaymentMethodKey
    suit_classes = 'suit-tab suit-tab-keys'
    formfield_overrides = {
            models.CharField: {'widget': TextInput(attrs={'class': 'span5'})},
            models.TextField: {'widget': Textarea(attrs={'class': 'span7'})},
    }
    extra = 1


class PaymentMethodAdmin(SortableModelAdmin):
    formfield_overrides = default_overrides
    inlines = [InlinePaymentMethodKey]
    list_display = ('name', 'method', 'enabled')

    save_as = True

    sortable = 'sorting'

    fieldsets = (
        (None, {
            'classes': ('suit-tab suit-tab-general',),
            'fields': (
                'name', 'slug', 'method', 'enabled', 'logo', 'picture', 'description')
        }),
        ('Configuration', {
            'classes': ('suit-tab suit-tab-config',),
            'fields': ('config', )
        })
    )

    suit_form_tabs = (
        ('general', 'General'),
        ('config', 'Configuration'),
        ('keys', 'Keys'),
        ('translations', 'Translations')
    )

    def response_post_save_add(self, request, obj):
        return super(PaymentMethodAdmin, self).response_post_save_add(request, obj)


# class PaymentAdmin(ModelAdmin):
#     list_display = ('date_created', 'method', 'amount')


class InlinePayment(admin.StackedInline):
    model = Payment
    extra = 0

    readonly_fields = ('amount', 'method', 'code')


class PaymentOrderAdmin(DefaultFilterMixIn, admin.ModelAdmin):
    list_display = ('code', 'date_created', 'user', 'sum', 'checked_out', 'paid')

    inlines = (InlinePayment, )

    default_filters = ['checked_out__exact=1']

    def show_order(self, obj):

        template = Template('''
        <b>Invoice data</b>
        <table class="table table-bordered">
            {% for idx, item in order.data.invoiceData.items %}

            <tr>
                <td>{{ idx }}</td>
                <td>{{ item }}</td>
            </tr>
            {% endfor %}
        </table>

        <b>Order</b>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Image</td>
                    <th>Title</th>
                    <th>Price</th>
                    <th>quantity</th>
                    <th>meta</th>
                </tr>
            </thead>

            {% for idx, item in order.data.items.items %}

            <tr>
                <td>{{ item.id }}</td>
                <td><img src="{{ item.pictures.0.image.thumb.url }}" /></td>
                <td>{{ item.title }}</td>
                <td>{{ item.price }}</td>
                <td>{{ item.qty }}</td>
                <td>
                    <table class="table table-bordered">
                    {% for key, val in item.meta.items %}
                        <tr>
                            <td>{{ key }}</td>
                            <td>{{ val }}</td>
                        </tr>
                    {% endfor %}
                    </table>

                </td>
            </tr>
            {% endfor %}
        </table>
        ''')

        return template.render(Context({"order": obj}))
    show_order.allow_tags = True

    readonly_fields = ('show_order', 'checked_out', 'paid', 'sum', 'code', 'user', 'total_paid')

    exclude = ('body', )

    list_filter = ('checked_out', 'paid', )

    search_fields = ('code', )


admin.site.register(PaymentMethod, PaymentMethodAdmin)
# admin.site.register(Payment, PaymentAdmin)
admin.site.register(PaymentOrder, PaymentOrderAdmin)


class InvoicePaymentAdmin(admin.ModelAdmin):
    exclude = ('payment',)

    list_display = ('order', 'amount', 'method')

    class Media:
        js = (
            'modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js',
            'modeltranslation/js/tabbed_translation_fields.js',
            'bower_components/chosen/chosen.jquery.min.js',
            'js/admin.js',
        )
        css = {
            'screen': (
                'modeltranslation/css/tabbed_translation_fields.css',
                'bower_components/chosen/chosen.min.css',
            ),
        }

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == 'method':
            kwargs["queryset"] = PaymentMethod.objects.filter(method='invoice')

        return super(InvoicePaymentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(InvoicePayment, InvoicePaymentAdmin)