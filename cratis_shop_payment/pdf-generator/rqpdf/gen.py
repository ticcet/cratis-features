
invoiceData = open('data.txt').read()

import base64
invoiceData = base64.b64encode(invoiceData)


from rq import Connection, Queue
from redis import Redis

# Tell RQ what Redis connection to use
redis_conn = Redis(host='172.17.42.1')
q = Queue(connection=redis_conn)  # no args implies the default queue

# Delay execution of count_words_at_url('http://nvie.com')
job = q.enqueue('rqpdf.tasks.generate_pdf', invoiceData)
