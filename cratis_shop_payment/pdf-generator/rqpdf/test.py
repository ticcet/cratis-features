


address_post_index = ", 13512"
delivery_address = 'Aleksandr Rudakov' + "\n" +\
                   "Oismae tee 62-32\n" + \
                   'Tallinn' + address_post_index + ", " + 'Estonia' + "\n" + \
    "Tel: +37253402505" + "\n" + \
    "Email: ribozz@gmail.com"

rows = []

rows.append({
    'id': 123,
    'title': 'Test product',
    'qty': 3,
    'unitPrice': str(33.50),
    'totalPrice': str(100.50),
    'deliveryPrice': str(12.90)
})

def _(text):
    return text

invoice = {
    'id': 100500,
    'date': '23.06.2014',
    'email': 'ribozz@gmail.com',
    'adminEmail': 'ribozz@gmail.com',
    'fromEmail': 'ribozz@gmail.com',
    'companyName': _('Inter24 Inc.'),

    'infoHeader':   "",

    'shippingReturnAddress':   _("InterCom Group LTD.\n" +
                    "Inter24\n" +
                    "Parnu mnt 238, 11622\n" +
                    "Tallinn, Harjumaa\n" +
                    "Estonia"),

    'infoFooterCol1': _("Logistikcenter\n" +
                      "InterCom Group AB:\n" +
                      "Parnu mnt. 238\n" +
                      "11622 Tallinn, Estonia\n" +
                      "Reg. 10113432\n" +
                      "Momsnummer SE502071470401"),

    'infoFooterCol2': _("Telefon: (+46) 812 410 605\n" +
                      "E-post: info@inter24.se\n" +
                      "Webbplats: www.inter24.se"),

    'infoFooterCol3': _("Bank: SEB Banken\n" +
                    "Adress: Kungstradgardsg 8,\n" +
                    "10640 Stockholm, Sweden\n" +
                    "Bankkonto: 52771027720\n" +
                    "BankGiro: 737-3459\n" +
                    "IBAN: SE8750000000052771027720\n" +
                    "SWIFT: ESSESESS"),

    'logo': "iVBORw0KGgoAAAANSUhEUgAAAK4AAAA5CAYAAACvbxAVAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QTIzOEU4NDkxQzJFMTFFMjgxODM4MjZENjc1MEM0RUEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QTIzOEU4NEExQzJFMTFFMjgxODM4MjZENjc1MEM0RUEiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBMjM4RTg0NzFDMkUxMUUyODE4MzgyNkQ2NzUwQzRFQSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBMjM4RTg0ODFDMkUxMUUyODE4MzgyNkQ2NzUwQzRFQSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgK3buIAABuOSURBVHja7F1NjBzHda6qnllyl7I0sE4OAnEE+RAgATwCfBIkcyjbCBIk5ipO4IOBcBnZTmJB4q5lx7FhaZdAbCdxHJJ2ZMOOYJIwcggQWGRy8CG2d2n4rtUhx8BL5OBDAnj0w/2Z6arK+62uac3szpIUKWCmyN7u6e6qrq766tX3Xr2qth9/+TWDwVpjIu0t/jWywyO6Hnkn9/IPS+edcdl1vmQ5njWfhB+PwdHfwP5XeBH/pXtNOuBnUBybn4J7swePCP/yx20zC9MXGjEG4xC00TFmYjSRIazQZADHCsAR7nECKgvxEXBRwB7xdktpnDDRfS0Y81BhjYfbv2xjfCtKknizPsdGbSRRnm8TYC0mOAGAZ2G6gkNYBMJLIEDGKHCFAwJlZKAxyAyDDOPIPXy5ule2xTLEl2+W/qGd0pudMjwD5y7ApQfzm2zU2NFIQiJtq3uiSvl0/yzMAkncvF9W9DjpunELfEO0jNoR0rfiGgEvP7kXzHf6Ib5vT/A4Z60DWD8971xwzi7DqW2bg5IkrDYMkbQ2vhd2fx8lDaYZBOD/Ael7blZ1Uw5cK8hA3krYNCx9b5E+/B6A9ts7Mb5vAGK8Lwl6qxI6/NmCcd5Z+wJc+j+VpGPow3tg9/QgBFMGbhcN52CzNyDOuRl9mAFXgJdUL/odIhMCxpQT/opHkX8TcCKrYZHA+9HdEL+/6+Nv9uH3IKKsRuphTangj7aAo79YKCy0C/c5SPNmzHhspfARjgMCFtKDBkBqmzlKGXa9WEnfWQ1OL1VQzSiaimraRB8UwAfQh48MfLyAoEV6UEKaZVSJGE0B2x6CnRKjtD61UIQdiPgiQPCNZGUYog8gqeHWfmDgFpAWSvHgRDGsTBizMJXAFbRa6arxwKpCZq0xI+iDGaYPvw/g+g6A9vgeAKtEwMVcVsNvw9aHPqQnDQEEtz270HDH4MKyie6mzU1q1AvYxJ9xN4C0jkaOX9jq/CxMtXKWWwxIVUrCdBR9cBV9eHzPh2/BdnwXJa0xJGlVZMYMwEaAiPegcYxBFz4133BvOhtehObyFj4k2ZORV4iJrBDKoTYMJePWzJA7teYwE3OKEJNZjDEhJqnMNJWZz/5wz8cfbvvwyC525yhtsSuX+0Jm7qL/cM1LWn1QuFDp2gHRvFuGs3DpH+F8C2Eeq/z0AZtvHrFMUpzmUE1zmflsFqYQuApKRGMcAnAUhJoEvuyeJ4AWXNwuy/ag9AzaKKBNm0kATiiLhsALDzVIK/YIvMFtl+HTcGkVTs/z0wnA/wua3PMNa95oAkcpBMCWOHKy/s7CdCtnmUavR+Ppwx8A6C6AtH14V5SwMgqYcPh3yPpg2cwmUtgJfSjhPF4fBH1ywHSfO9qwx4CA/BWc6FkbQR7Hf25aF0Eh+4cy2AcaqjOKlSHOdLMZcHW81pphXqt0QWy4IGnDt0FKtveSyYtlIEcPwoBrgxOG7cRBhooN8WVL3LUfRBkjrc99+mjD7RbGfhFi7nC88PIRiNtw5pvw435nzNCI2ixMKXB1eFclpMrdhFmbxNzJvo//BJy2vStd/iB12pYsu1G68igqHO6LrEuP0hjoeZatD2jY3TOq+VHcZxYKCzqcWwOQvsHGjvgyUIU5OPwGJNDMrQ+zMKXAJSlL3bkZyRqFPnwYJOMPQNI+VNEDASMOBtjMxyE527Dsxb/70QcFIJnKTDKVrSw0zDzc+zxs23Lbd+A5cxDrE0ZG9GawnWaqoJxRRqOGumFG4JODEL+77eNDaD0YRB5C8BlnzYeBTfqtdGEy+oAxcKRsLyZT2Z/PF24PUPwi3PGGDDq8BLv/BvpA5jFrZiR3xnEzMFUKUPwISNrv74T48HZgSet1cMBUNlqb0QweED48fUCrRAN+92Ny8kFYP3u0cEBnAnLem5DyAK78hww/5IbmicLfff3r6OBzSn5uwbaCiuAMBvcufPFLX7pF4AYZvnUmdeHsC24fA0578aaPD+8gnw1RJC1bBNgXNyYAO6ELLtGFw9MHL89GO29g4OPtnwXwbsPPNRvDdt33d9Lw9a9+dQl2570P3LggAedcG86dvMN10YIN0928BzjowNaF7YHIeyxPzMcN2K5KY50kjVMjzmMal99FElccv4NIMUezELp9H74Lkva3QBljTissQB1yKocu4auRXRPjbdIHRST6Keww58UHPH8UXcOsfQEu7nCDcZLCZMF7fzySp1ng3gQ9zYqiewfKsCsV3YG36FgG7sY70CD2C9goV0Pw7RDisP4A+cM6Kpw7D++8BqcOcgm9BOl0NB2KWxS433iXATcOYyuYx/smvrzj4yPbUMn9xGltGsHKu3lnYgbAO0MfgtiN+2LnZVOZ+RyCFw6+AtJ+28RwKOWsLMvXkU/7smSbMov8rTtQhicg3WVM11C6jir6LgfoSXzLY08lDVN1F6V/DvIEDXUN8ref5FwtfdnBXilkDRxAb5rN5rvMqjDkB2A/3I/he9tleGSb/AosmayiNQl4QTipy4aA4xj6oB5nt0IfsKFg9fdxGMJRi8LUVhYKV8CZr0CcNw/jjzsoy8uQ9gmo4EV65+C24FFP3YlCLAcDU3rPbwXsptloEFDuYtiEhtNNwFUzZdSRSxAMUIalK8xcs7kK5TYKuG0om7UBvAumEWVQyUEcbIhR3u1dRRUMzRGzjw+8/95bIGl3xcuLR8QsASfqLIXMcywKEFXa6rHSB3eb9MGLmqYjbGIwexbAG4FAfBlu2p70Rb/6ta+hEvY2oP71F74wKR0wwluHlDkELDQKpCL0kihxAxyjhLKTpbk1IfccAplsG/K750OofEoi5DOGHugMXcwLgrfEQRwEYIxtlKD1APdc0gaY0iLgeoqHiY6Kd0j+3Zv0XaFe9B3N337jGxsj7bggcZ/oR4/Wg0fQcXvPKHBE6SI/WFdhToAXpdtX+mBlROxO0gdveJCCR9gw/YAJPDtfWM+c10wE3ueXl7vwTuvQFXKFWOrSlYsiiNapTYag6uJl2PfgcFnfDeNAo7wg1gis7AgUBGkISCkvrctB5YP08z5ChWMlPVyrvFV4h8XkyMTpblmWgjn/XIVtTX0/pOVCXu1pUCyXMjO7NqhFvB+2K3ByC8EKoF31vlzzooBzHrEui05NeVwelINuWTJojc41pNdxVCZUt4eXuKuQzBLEb6c5i/jPuZ7lRndFlMa8nlbhfZegjNrKBVfOPodlhPed++aFC5RvnAHxxCDGHwCnfT9yWhzGVXoQsrKxoumjCIwyXHu36ANaG7Br2CNqAOApPTzKfe5Yo4ifuPJfL/zr6d/eOZAqgDTBSmGOGxm4jYL2pLzh+4m0ke5gqZ4Gd5tuGdjKdSxwkk4egavxGOBwHdKCZzWKtqsqexEq4xLwx1aoniFxXBvSXYN4KGHOJIUSgOZLnzkq2VcA4C3m0Vg2TgF4RbatGoV5feCxNwjcCwZHwAku5KAFbuxXy5LvE0cpkIzorUd5g3g+NdxDhEuQ7lLiy1I2AtwWNOpFfJccuM898wwIFt/NqYq2T3jnRVSm4Z6T33rppU0cgHhpz4f378ADdsnDS51gjFCDKBKzkr6OXt4mKnA79EGm/RxIH7zlEbY9aUAI+ML7548yl/z8JMDFysGKjNTtAQh9QV07ZqRUkEg3aXWoO1YQQ4UO+R7EPQVS7yo1BAJvBVySBgWCDSusgd1rlzio95fgGa0g5jjt4sV8ZgruAZaKRuMaPPYq5oNBh3mKOu+/RXmAvDcalHYroxtDAe5u9wf9VU+9ASpZljpYa8uNkOkGSBFA2ub52oJnXQ3Y00g5BAG8m5wqdCDdJRYWnjBjBAOWzZDGF5T/lO+//MxnsPF0qSwr0KbyEetGa67RvAQ/H230Q/ydHbQe0KhYHDJ5oWQN1NIY0DYH5x2iDzn/nYQ+NMXa0IAzADvTtHFlEuD2oRBxI4lbmcNY8UDgosRB8PpKOqCESS6eWngI+EaDeCI0rA24vw3UoJ3HM7HRgydskg8ydIsAwiVIv4WVkqWF+b4Kv3+pPYBYJE7DnoCreQohVnmifDeYyjl3IuO5Q7ZkeNYrAJyWSjuMh7ZrEEo5HekCP1/UMpGZ1Ctw9AGkB9j48FzDF1S3h7CWtLEn6g84/zIyuwbbNewlIC+nQxG6BVMpCju7u8v6vtJgtiA3j0LZnEfqoA3HN8vO02fOdBrwYn0omCMhhgTUIg6DzqqWmmZB3Dv6UMJfqDYaHi4tOqRPZloY9PugRLHUTbOaQWoFKSiUtqqYWKnoYMOjcHgari+HwN0tFDjbSQvywT8ZsJsdAI9EUJL2Tjx40wV7kiQyd/nnB4OSpDkNcbPkoQEBSGsTto5KI6Avi5gGglWleRAlmZSjAgSGtT3AxSakfX0UaOH+dchTpyTQ8Ls2mw2kL2vB9zcyinCJwRKU92/ArdCYzAdCZqEI0rgbh7CUIAAhD1SmAsQ1eG+kBxvwjitY7sEzcP/0k5/EBtQazovd+Lcf/aj3Jx//+DUEblTFF643G+FUg3shmdyoVgQjkjbq1Eir/mJvow8kke4yfShp8CFSo4rDZGgfO+6AzVYqca1jKhJYY0aHeO6WA3WrLhCP3BTptaxmJVVwALk6sIHKWGoQCGhrzdDgCFRep98fJGki0u+SvH9bhB31AgSYgvdKQfCYu+miVzi7AtLwchlGvjbactchLx1fAQbeAxsoxImDIeUPeqA2KZZsy+9BmZzBd4+BZ8IoiArvuIE35yadn7rJNKo0atWQWsQGCnkDPQEUY+i9sNfZ3N3b65Rlmcpfeoju4sc+ts5UQQSZrZT8hs4kiMJvY81cpQAeRx+K5G9wMH2It0AfqjkZdfrAjSZMOAqBEg8510CAq9xW+WPik9Dlu8IRJUizNkJlIgKNnIdH5MFeunQGbqSGQH1UzIFbtlDao1TX4WYAaVeVHbFiGFw2BbluCJw+9wDMETFP8P6bwcXLIZSju+cA9EBBG6SbLyg/l721Z3xu/gphuY95qu5tATB+abVchfPSMD4Ox/vQhXMRuXVmzRgXUJKeQV7va+Y1tejA+3ShB1n/o1OnHoVrD1Dj9xUtAi7fxiH53JGq8qNRO67M2CVJG7NFOgi0d44+WDOcVqyNvkmehuiDapV1+mBstUzUZAMQAwJvWQNuIXPsypIlJ4LEInBwmCM6kdZeuBo6XYppSGd9YFxJl2iA8yIEQlocEK5twT1t7fZR2kOlbUIeekOVgs/F9IM1zHErZbEACe8xz+UYZQgkLbwfd7eRxx4dWRLiGYh/ue6yShJdehmtHW1EUecOatmSFx9LfiwHUdLQ6nJ8H3+Gy5D2BrzDKsRDU2Q7iJlNLS+QDlpI0Ix3Iym58kxowD24tpkWWBQqJTm8QQuC8MRGHmUhOWN5kY+QZOX+9KFabaGiD2Yf+hBG0QcBcJ0+uDH0IVGOyYd82apAwOWuVxWvSFShTKYnukbnC3qGKm0Yjws8nzofU9oKGKgoAJLH7vABeJ9zcA26To9KHEsU69EmuglS54xKXcsS7yxgo8WafKUwRqFzReFMfLtnURvt00BFWtrdpvkqUPGgxByH/KxWdIPCuZgD01QTUBFcasJKZiyXezSl8jmNgFQJSeY559SfoQPXX4W8AHjDOUgTG0+H8xEXMXNohkdzJJTnA6hgUkP1lYDwRHHcys/WN5Lp7nc/+tEuXDyLekEjplm5Qhckj048tYOZgD5k/FfpQ5hw8OKW6YPQlkk9xLRSVYNGikqLSOp7e58UCTK4w1t4aRgqDQg0BXb9QwB6jfmgghvpQmhBGucJ5IWDiovXoJtdVDBwgw1LIFm7FhUiAB+AtetYsduyzGOr58pbowR2LozQ4EOrL/y9spmiBMcGFDoWzjdKR3Zo4tfOXlQnIGuHl3uNngoaQblUSXv07XeoRK7oO3iV2MqDC7Z2kA8Ic22kG0toywWwbkJCm1AOLebdhpZbtJ6k6Ov/+ZOfbn7kySdBUQ3tKLZkuKcVgnv1Q088cdVxJtEaAVIbwe6uNdIUnYwv0tJKstyns2+nD1HmztRNXoUCcQL6YG6TPkTJUzVkdTBw2YIgBRN4KVNtqFRJUlE4xI0802rjFYN4iqdeZhw24HSPptdjVxh46amBYZBDajhIcBlSOgspdrhhsICAPwBYALAVQz9JezgXVXEUk5T0UqSJj2BGdB/RHH4HbfgozdUigQOOCHrkvNALdsaY0ZQZnJA1BRKBQOUN0t7Q96aeSHgpnyi4l2bgniBTYNWYO7JVDYS7fUxTBiDiCjTbVyg1eneab4h/FoO4vaJFhfWLcNxF5bfi3EJdpcxMt/I78VuhE44GBsSJQ7pLIxUbJK6pnePp7tBdBY5r0/oIgSKoYpjnR+Op8miD7M3QtYms4l4KmPil1ak/UTRZnzghcymnXb5w0ioeSpTEx3jrQfwVNoO5atFryR95bTG3PQlxr6IU5sGAxCiT1s3ac9wIyfpg0zNHPHeoJ2DlzopEdbVRrmrpAKYCvjMuLU2vkHT4nVwqq2SiE3lhTTUTRdLG7TU43srfUdfs1jxC+tjYV37ys3W6D/ZX4b6nQHL36LnODtMYk6w6PUy/IcSGgKCzG1yM2XDvO0sfcuedw9EHRLEzZkKaCy1/S8byZVUdOncj04LX8LwT4GUjYUPxmNPFG7XkkddtoKLheFCgJWnkUk2dfDo48gZpdKvBCFLSNiHta3BeOd11zRM3opHPHcp7gWsKugPLwUzg5E7PttLAJV792VfknTMJnO65KlsH0jhVcLcuTkWWHNvhfeD6sFvp+vWfX/3Q449vQJxFiHMCqYPSHowHcV6DOFd//otf9Oxjqz/e/XUZjryFi3NE8nMkYBaSYwYbg0ZNX04mNhapS2JFrTDDS/LTOfntrKYlNEN/uypu1LFsWfK0yJ8ve32RJmwLDWfuaxSDH3/2g3NmFqYqNHIbbhBJG7J5MYXw1UKcCofst5n1oSDJbJP57HasD1bMDftZH4hmh9kq5dMLXJkBwVyWnVn0Gw8K4LtGH6xNpreD6IPNuPDthAsXLt6Lckfl6LRaJcy7aErMYcPy8tl7A1y14wbxfSXrgOXp50VkMDXEqbsCsD2U9UFB5w6yPqhSRnbCA6wP0ZohX9XDgZU8tqDQc6fwlgBq4wAH6I5y2BrPrHtoaXpmhBN6L0ujN8ZJ3Oyn+Y/IVzvLS/3Zef66I/I7Kq127fn18qHfWcPfgvLcgt8jywev3UngOjXiO7UYGPbHpU0ksVIIG3nFxcNaH9whrA+8oA2bdQ60Pmj8w4N2fYS/bUfO18P5TDoaOUYn71fQ51SOu2OAtS7XVzNgtSVuV+65VIt/Wq7jc389Ju1x+To95tl5GusSNwfkqyPSWq+Bf7VWPuOeMWn53D7HtSJRvaqHMp7uQao1zf70wVnmv7JirozFKxc1ycUR+auzyemf0O14CIDBmE1vz+lDHDF1SP0UblHinhbJgn3chVsos5UMACh9Dpo1W5/tuyrPfyoDIFbwwzXpfDID58Yt1u+4mcaL0oC2sgbcGXEflpE6tuf3bWZU4WSNOqyIgKDygd/vyIdmnCz8wSAUKetU0hJ9YAlsBdjKdedkeBjBhyo9Lkx3FAC2gBscH0OtH7r8BSvnINEjsD9C5+DY8MJlTcPpNJNdNma2Xx07Z+mbS2jFazyctG1JBSBoWtKtvdMBgbpcA01OrM8JiFo1KdiV7bV90u6Okar5s1dr6SoFWczAuTni+fk9S0IlenUaAGW4itvd5rhOR8wYmLpaTQbSyBaDUfQBw7xlQN4P2320d+ZY4cx9QJSPAVjvB1Qeg/PvgXN4TwvOLeA1+H1EAGwzsE5KH3gfD8sUFqXwT8n+bmgWXTO8wEad//VGnG+LRO/dYcVNuewVeXfloysj7rsm9y5J/i+ae7PIyWiqEDJO6uX7CqyYWaIIZgx9QKSzpDW9eWefO1q40BS/M3X+1pEVnbYRTRoXjyUc9kModo35ZjPEB/dkqC/WfH/H0gfrkvPJIcJZU81xuiqV8k4vw1TvrlGS5TMXlszbZ79uCnAOkmQHUZVR1zalMShFuVxT7vL7LmYAPyMAbteowT355lxDlZ8gEhYHP5nXRgLpnHDcpkpasTjg5Gvs6uesvQnS84c61GlkZRzlosmtS3wSo3zrFwc6ePqLX+sb+yBaiTUP8QDrAwM6JIfJCWlCRyrgpFoT4NyiSOHLte5VR4ZyTRzDnViG6JxI01YG3FGVf0GA8oq59VVx9F2uj+DJl4XCPGWqZaPMiHvOy16pwvE6VdBnQLlu3DXgRnFpVH5biKJG/lEifX20opiJiUzsqQV/xknWWq4WiLbpkBcbcWk182yaugxlAuBd0+CoXaz5/h4weBFlKnmY+F2xcs7UTGArGYAQpGsj4l0xb/c7rQN7VBiXHlbuoxl9OGOGp2hflwZiBFRnM0Vqv+dfn+DZGs+INL2RpbFWy+OWAPVMRhEuTviMScrntoL94Jf+fff10h/pgUjDFRnRDxLBOqcWBMOjZ7QXWyqG++Da/cBT39ts/OpIYX9DP0qd5KLNfNf3oQ+7Pt646cNDb/pA6zkYUfhCNv1Fh4916BmvN4U3v6fhBj/9/IdmQ75TFpx+Mo8WVzbsaKMLcZTyoRGfJDIrb06UucLkXzyvpK6uW5U68Zjdl+yw/CUeVcx4MY5qMESXEqg+ds2Kmos68yEmB/NZmL7Q0GVmq29HsvVgTr5NNo4+JLdC+T5EchDOPhptJqAPJg2AsGI2zvehTh9S45j5KkwncK1RHwSWsAgW7HdLUcZYORP6IMqbdv8hNzqYSrym6TdKH2oAtmlGg03rXamTj5NpEnXfh/rUoUgr7s8+GjW1VMEITdD5Zi7yN8v6cnEcfdCJ7Nr167E6UE9KH2JGFUJyIM+mE42jD/pvhtvplLixmvNOIDWimBn5fGlDHavlQ32FSF8Ge1qeNPtsHktS/dr6QfRBwZ58H8yErpMxfWZqVotTyXEFbqVwWQx9tSbU6IMR6TtnTPIQS59BE1wyV47JM+wg+qD+X6q4jXadHEMfUsOZhamjCjYzL6ijTSGeXrrE5yj6oN/qrXxiK6qgAJuEPqh1IHmZibTXod5x9CH/9OosTKVyhtTAmnmb/LaGEK24KNKXJhm8R0z1fV1V0NBiwMCzSfoeRB+smDbmLQ90LBi11Wb7ZMON6fcczbGS4edZmD7gzjln52Uts3l1Aq86+AqYMlSrv3EAYN46BJCtJKgdMnnV6YPJZkEofYCWY4848UbQhe/siMGK1KDYFDbnyE8ijdzNwvRx3J2FwoVG7UuNQ8fWmuHJzvRtXQCtQ1DuuKzb1xm0DEwjNluTRtyU/7oki83uQmF38aM6CxlXTrMiBKiaDx2VI5dI8tONflaN0xf+X4ABANTGBDVY5iqtAAAAAElFTkSuQmCC",
    'billAddress': delivery_address,
    'shippingAddress': delivery_address,
    'subtotal': str(100.50),
    'tax': '20',
    'taxTotal': str(20),
    'total': str(120.50),

    'rows': rows,

    'texts': {
       'sh_invoice_no': _("INVOICE No. "),
        'sh_invoice_from': _("From:"),
        'sh_invoice_to': _("To:"),
        'invoice_col_id': _("Product ID"),
        'invoice_col_description': _("Description"),
        'invoice_col_delivery': _("Delivery"),
        'invoice_col_quantity': _("Qty"),
        'invoice_col_total': _("Total"),
        'invoice_label': _("INVOICE"),
        'invoice_date_label': _("Date: "),
        'invoice_no_label': _("INVOICE No. "),
        'invoice_bill_to': _("Bill to:"),
        'invoice_ship_to': _("Ship to:"),
        'invoice_row_no_label': _("No."),
        'invoice_notes_label': _("Notes:"),
        'invoice_subtotal': _("Subtotal: "),
        'invoice_tax': _("Tax: "),
        'invoice_tax_total': _("Tax total: "),
        'invoice_total': _("Total: "),
        '$': _("SEK"),
    }
}



import json
import base64
json_data = json.dumps(invoice, indent=4)
invoiceData = base64.b64encode(json_data)


from rq import Connection, Queue
from redis import Redis

# Tell RQ what Redis connection to use
redis_conn = Redis(host='172.17.42.1')
q = Queue(connection=redis_conn)  # no args implies the default queue

# Delay execution of count_words_at_url('http://nvie.com')
job = q.enqueue('rqpdf.tasks.generate_pdf', invoiceData)
