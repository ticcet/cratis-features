import hashlib
import json
import urllib

from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.utils.encoding import smart_str
from django.utils.translation import get_language
from django.views.generic.base import TemplateResponseMixin

from cratis_shop_payment.common import log_payment, get_client_ip
from cratis_shop_payment.models import PaymentMethod, PaymentOrder, Payment
from cratis_shop_payment.views import PaymentView


class FakeStart(PaymentView, TemplateResponseMixin):

    template_name = 'fake_payment.html'

    def get(self, request, *args, **kwargs):

        method = self.load_method(kwargs['method'])
        # settings = method.behavior().config

        # payment = get_object_or_404(PaymentMethod, slug=method)

        po = PaymentOrder.objects.get(pk=request.session['order_id'])
        # order = json.loads(po.body)

        return self.render_to_response({'order': po, 'method': method})

    def post(self, request, *args, **kwargs):

        method = self.load_method(kwargs['method'])

        po = PaymentOrder.objects.get(pk=request.session['order_id'])
        po.checkout()

        payment = Payment()
        payment.order = po
        payment.amount = po.sum
        payment.method = method
        payment.save()

        po.pay()

        return redirect(request.session.get('payment_done_url', '/'))