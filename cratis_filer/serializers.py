from django.conf import settings
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.files import get_thumbnailer
from easy_thumbnails.processors import colorspace, scale_and_crop, background

from rest_framework import serializers


class Size(object):
    def __init__(self, width, height, crop=True, upscale=True):
        self.width = width
        self.height = height
        self.crop = crop
        self.upscale = upscale

    def as_dict(self):
        return {"size": (self.width, self.height), "width": self.width,
                "height": self.height, "crop": self.crop, "upscale": self.upscale}


class ThumbnailImageField(serializers.Field):
    def __init__(self, sizes, **kwargs):
        self.sizes = sizes
        super(ThumbnailImageField, self).__init__(**kwargs)


    def to_representation(self, value):
        try:
            data = {
                'type': 'image',
                'id': value.id
            }
        except AttributeError:
            data = {
                'type': 'image'
            }

        if not value:
            return data

        tags = []

        if hasattr(value, 'tags'):

            from shop.serializers import ProductSerializer
            for tag in value.tags.all():
                for ref in tag.refs.all():
                    tags.append({
                        'x_percent': ref.x_percent,
                        'y_percent': ref.y_percent,
                        'product': ProductSerializer(ref.product).data
                    })

        data['tags'] = tags
        data['name'] = value.name

        try:
            thumbnailer = get_thumbnailer(value)
            thumbnailer.thumbnail_processors = [colorspace, scale_and_crop, background]

            for name, size_def in self.sizes.items():
                size = size_def.as_dict()

                thumb = thumbnailer.get_thumbnail(size)

                data[name] = {
                    'url': thumb.url,
                    'width': size['width'],
                    'height': size['height']
                }
            return data

        except (IOError, InvalidImageFormatError) as e:
            return {
                'type': 'file',
                'url': value.url
            }



class ImageFolderSerializer(serializers.ModelSerializer):


    def __init__(self, sizes=None, **kwargs):
        self.sizes = sizes
        super(ImageFolderSerializer, self).__init__(**kwargs)

    def to_representation(self, value):
        images = []

        tb = ThumbnailImageField(self.sizes or {
            'thumb': Size(334, 220),
            'thumb2': Size(500, 500),
            'medium': Size(800, 600),
            'big': Size(1024, 1024),
        })

        for file in value.files:
            if file.file_type == 'Image':
                images.append(tb.to_representation(file))

        return images