from django.conf.urls import patterns
from cratis.features import Feature


class AngularJs(Feature):
    def configure_settings(self):
        super(AngularJs, self).configure_settings()

        self.append_apps([
            "djangular"
        ])

        self.append_middleware(['djangular.middleware.DjangularUrlMiddleware'])

        # self.append_template_processor([
        #     'cratis_shop_regions.context.region_context'
        # ])