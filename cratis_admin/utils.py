

from django.db import models
from django.forms.widgets import TextInput

from suit_redactor.widgets import RedactorWidget
from django.contrib import admin

default_overrides = {
    models.CharField: {'widget': TextInput(attrs={'class': 'span5'})},
    models.TextField: {'widget': RedactorWidget(editor_options={'lang': 'en'})},
    # models.ForeignKey: {'widget': LinkedSelect},
}

def fix_translations(modeladmin, request, queryset):
    for obj in queryset:
        modeladmin.fix_translations(obj)
fix_translations.short_description = "Fix empty translations"



class DefaultFilterMixIn(admin.ModelAdmin):
    def changelist_view(self, request, extra_context=None):
        ref = request.META.get('HTTP_REFERER', '')
        path = request.META.get('PATH_INFO', '')
        if not ref.split(path)[-1].startswith('?'):
            q = request.GET.copy()
            # use try/except in case default_filters isn't defined
            try:
                for filter in self.default_filters:
                    key = filter.split('=')[0]
                    value = filter.split('=')[1]
                    q[key] = value
                request.GET = q
            except:
                request.GET = q
            request.META['QUERY_STRING'] = request.GET.urlencode()
        return super(DefaultFilterMixIn, self).changelist_view(request, extra_context=extra_context)



