from cratis_profile.models import Address, User


class LocalProfileAdapter(object):

    def save_address(self, user, data):

        if 'addressID' in data:
            self.delete_address(user, data)

        adr = Address(user=User.objects.get(pk=user.id))
        adr.update_from_data(data)
        adr.save()

        return adr.to_data()

    def delete_address(self, user, data):
        if 'addressID' in data:
            adr = Address.objects.get(pk=data['addressID'], user=user)
            adr.removed = True
            adr.save()

    def get_addresses(self, user):
        return [x.to_data() for x in Address.objects.filter(user=user, removed=False)]
