from django.db import models

from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    firstname = models.CharField(max_length=255, verbose_name=_('firstname'))
    lastname = models.CharField(max_length=255, verbose_name=_('lastname'))

    def __unicode__(self):
        if self.firstname or self.email:
            return '%s %s <%s>' % (self.firstname, self.lastname, self.email)
        else:
            return self.username

    last_order = models.ForeignKey('cratis_shop_payment.PaymentOrder', related_name='as_last_for', null=True, blank=True)


class Address(models.Model):
    user = models.ForeignKey(User, related_name='addresses')

    city = models.CharField(max_length=255, null=True, blank=True)
    country = models.CharField(max_length=4, null=True, blank=True)
    address = models.CharField(max_length=255, null=True, blank=True)
    address2 = models.CharField(max_length=255, null=True, blank=True)
    postcode = models.CharField(max_length=10, null=True, blank=True)

    removed = models.BooleanField(default=False)

    @property
    def title(self):
        return '%s, %s, %s' % (self.country, self.city, self.address)

    def to_data(self):
        return {
            'city': self.city,
            'country': self.country,
            'address': self.address,
            'address2': self.address2,
            'postcode': self.postcode,
            'postalCode': self.postcode,
            'title': self.title,
            'addressID': self.id
        }

    def update_from_data(self, data):
        for attr, value in data.iteritems():
            if attr == 'addressID':
                continue
            setattr(self, attr, value)