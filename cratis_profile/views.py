from django.views.generic import TemplateView

from djangular.views.mixins import JSONResponseMixin, allow_remote_invocation



class ProfileView(JSONResponseMixin, TemplateView):
    template_name = 'account/profile.html'

    profile_adapter = inject.attr('profile_adapter')
    order_storage = inject.attr('order_storage')

    @allow_remote_invocation
    def save_address(self, data):

        if not self.request.user.is_authenticated():
            return None
        return self.profile_adapter.save_address(self.request.user, data)

    @allow_remote_invocation
    def delete_address(self, data):
        if not self.request.user.is_authenticated():
            return None
        return self.profile_adapter.delete_address(self.request.user, data)

    @allow_remote_invocation
    def get_orders(self):
        if not self.request.user.is_authenticated():
            return []
        return self.order_storage.get_user_orders(self.request.user)

    @allow_remote_invocation
    def get_addresses(self):
        if not self.request.user.is_authenticated():
            return []
        return self.profile_adapter.get_addresses(self.request.user)

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)

        context['user'] = self.request.user

        return context

#
# class ProfileEmailEditView(UpdateView):
#     template_name = "account/email_change.html"
#     form_class = ProfileEditForm
#     model = User
#
#     def get_form_kwargs(self):
#         kwargs = super(ProfileEmailEditView, self).get_form_kwargs()
#         kwargs.update({
#             'request': self.request
#         })
#         return kwargs
#
#     def get_object(self, queryset=None):
#         return self.request.user
#
#     def form_valid(self, form):
#         data = get_client().get_user(email=form.cleaned_data['email'])
#         get_client().sync_user_to_erply(data)
#         return HttpResponseRedirect(reverse('profile_edit_email'))
#
#
# class ProfileEditView(UpdateView):
#     template_name = "account/edit.html"
#     form_class = ProfileEditForm
#     model = User
#
#     def get_form_kwargs(self):
#         kwargs = super(ProfileEditView, self).get_form_kwargs()
#         kwargs.update({
#             'request': self.request
#         })
#         return kwargs
#
#     def get_object(self, queryset=None):
#         return self.request.user
#
#     def form_valid(self, form):
#         data = get_client().get_user(email=form.cleaned_data['email'])
#         if data['customerType'] != 'PERSON':
#             data['companyName'] = form.cleaned_data['company_name']
#             data['code'] = form.cleaned_data['company_reg_nr']
#         data['firstName'] = form.cleaned_data['first_name']
#         data['lastName'] = form.cleaned_data['last_name']
#         get_client().sync_user_to_erply(data)
#         return HttpResponseRedirect(reverse('profile_edit'))
#
# profile = login_required(
#     ProfileView.as_view(),
#     ProfileEditView.as_view(),
#     ProfileEmailEditView.as_view(),
# )
