
import os

all_features = []

for f in os.listdir('..'):
    if f.startswith('cratis_'):
        if os.path.isdir('../%s/docs' % f) and not os.path.exists(f):
            os.symlink('../%s/docs' % f, f)

        if os.path.exists('%s/index.rst' % f):
            all_features.append('  %s/index' % f)

with open('index.rst') as f:
    alist = [line.rstrip() for line in f]
    start = alist[0:alist.index('.. features') + 1] + ['', '.. toctree::', '  :maxdepth: 3', '']
    end = [''] + alist[alist.index('.. features end'):]

with open('index.rst', 'w') as f:
    f.write('\n'.join(start + sorted(all_features) + end))