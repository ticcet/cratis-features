from __future__ import unicode_literals

from django.conf import settings
from cratis_shop.templatetags.shop_navigation import _categories


def global_vars(request):

    return {'request': request,
            # 'categories': _categories(),
            'user': request.user,
            'settings': settings}
