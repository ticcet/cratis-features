from __future__ import unicode_literals
from celery import task
from cratis_shop.shop.api import ShopFrontend

# from cratis_erply.api import get_client
#

#
# @periodic_task(run_every=timedelta(seconds=30))
# def erply_basket_sync():
#     unix_time = int(time.time() - 300)
#
#     actions = get_client().fetch_many(
#         'getChangedDataSince', {'salesDocumentsChangedSince': unix_time})
#
#     for action in actions:
#         sync(action['tableName'], unix_time, action)
#
#
# @periodic_task(run_every=timedelta(seconds=1800))
# def erply_product_sync():
#     unix_time = int(time.time() - 300)
#
#     actions = get_client().fetch_many(
#         'getChangedDataSince', {'productsChangedSince': unix_time,
#                                 'customersChangedSince': unix_time})
#     for action in actions:
#         if action['updated']:
#             sync(action['tableName'], unix_time, action)
#
#
# def sync(x, unix_time, action):
#     unix_time -= 100
#
#     if x == 'products':
#         _products(unix_time, action)
#
#     if x == 'customers':
#         _customers(unix_time, action)
#
#     if x == 'salesDocuments':
#         _sales_documents(unix_time, action)
#
# #
# @periodic_task(run_every=timedelta(seconds=15))
# def add_shipping():
#     unix_time = int(time.time() - 300)
#     try:
#         sales = get_client().fetch_many(
#             'getSalesDocuments', {'changedSince': unix_time})
#     except Exception, e:
#         sales = {}
#
#     for sale in sales:
#         if sale.has_key('rows'):
#             weight = 0
#             price = 0
#             order = Order.objects.get(order_cookies=sale['number'])
#             for product in order.get_products():
#                 price += float(product.count * product.price)
#                 weight += float(product.weight)
#
#             weight_price = settings.PRICE_SHIPPING_KG * math.ceil(weight)
#             get_client().fetch_many('saveSalesDocument',
#                                     {'id': sale['number'],
#                                      'price': weight_price + price})

#
# def _sales_documents(unix_time, action):
#     try:
#         sales = get_client().fetch_many(
#             'getSalesDocuments', {'changedSince': unix_time})
#     except Exception, e:
#         sales = {}
#
#     for sale in sales:
#         if sale.has_key('rows'):
#             order = Order.objects.get(order_cookies=sale['number'])
#             order.remove_products()
#             for row in sale['rows']:
#                 product = Product(order=order,
#                                   count=row['amount'],
#                                   product_id=row['productID'],
#                                   price=row['finalPriceWithVAT'])
#                 product.save()

#
# def _customers(unix_time, action):
#     customers = get_client().fetch_many(
#         'getCustomers', {'changedSince': unix_time})
#     for erply_customer in customers:
#         get_client().sync_erply_to_user(
#             erply_customer, erply_customer['email'])

#
# def _products(unix_time, action):
#     frontend = inject.instance(ShopFrontend)
#     products = get_client().fetch_many(
#         'getProducts', {'changedSince': unix_time})
#     for product in products:
#         # update cart
#         Product.objects.filter(
#             product_id=product['productID']).update(price=product['priceWithVat'])
#         # update erply
#         frontend.reindex_product(product['productID'])

#
# @task.task()
# def celery_sync_products():
#
#     from cratis_shop.shop.frontend import CratisShopFrontend
#
#     frontend = CratisShopFrontend()
#     frontend.reindex_products()
#     return 'Success!'