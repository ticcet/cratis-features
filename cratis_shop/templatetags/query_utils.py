from urllib import urlencode
from django import template

register = template.Library()


@register.filter
def get_at_index(list, index):
    return list[index]


@register.filter()
def query_toggle(key):
    def toggle_value(qs, value):
        if key in qs and value == unicode(qs[key]):
            del (qs[key])
        else:
            qs[key] = value

    return toggle_value, key


@register.filter()
def query_set(key):
    def set_value(qs, value):
        if value:
            qs[key] = value
        elif key in qs:
            del (qs[key])

    return set_value, key


def encoded_dict(in_dict):
    out_dict = {}
    for k, v in in_dict.iteritems():
        if isinstance(v, unicode):
            v = v.encode('utf8')
        elif isinstance(v, str):
            # Must be encoded in UTF-8
            v.decode('utf8')
        out_dict[k] = v
    return out_dict


@register.simple_tag(takes_context=True)
def query(context, *args):
    """
    Tag generates query string updating current query string data with
    data procided.

    Exmaple:

        <a href="?{% query 'page'|query_set 2 %}">Next page</a>

    Syntax is following:

        query arg1 val1 arg2 val2 ... argN valN

    arg1 is query key name. It is static 'name' or expression.
    Each arg should be filltered through |query_toggle or |query_set filter.

    |query_set - will set this parameter or override existing one
    |query_toggle - will remove value if it exists and put it back if not exist.

    """
    qs = {}
    for key, val in context['request'].GET.items():
        if isinstance(val, list):
            qs[key] = val[0]
        else:
            qs[key] = val

    for key, value in zip(args[::2], args[1::2]):
        operation, key = key
        operation(qs, value)

    return urlencode(encoded_dict(qs))


@register.filter
def get_range(value):
    return range(value[-1])
