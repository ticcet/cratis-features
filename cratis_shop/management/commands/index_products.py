from cratis_shop.shop.api import ShopFrontend, ShopBackend
from django.core.management.base import BaseCommand



class Command(BaseCommand):
    help = 'Import data from erply'

    def handle(self, *args, **options):

        frontend = inject.instance(ShopFrontend)
        frontend.reindex_products()

        backend = inject.instance(ShopBackend)

        print('Pictures ...')
        from cratis_shop.management.commands.download_pictures import download
        download(backend, index='shop')

