class Product(object):
    """
    """

    id = None


class ShopFrontend(object):
    pass


class ShopBackend(object):
    def fetch_products(self, changed_since=None):
        """
        Returns product generator

        :rtype: Product[]
        """

    def fetch_categories(self, changed_since=None):
        """
        """