from django.core.handlers.wsgi import WSGIRequest
from mock import MagicMock

from django.template import Template, Context
import settings


def test_simple_case():
    request = MagicMock()
    request.GET = {}

    context = Context({
        'request': request
    })

    out = Template(
        "{% load query_utils %}"
        "{% query 'boo'|query_set 123 %}"
    ).render(context)

    assert out == "boo=123"


def test_query_set_unset():
    request = MagicMock()
    request.GET = {'boo': 1, 'foo': 2, 'baz': 3}

    context = Context({
        'request': request
    })

    out = Template(
        "{% load query_utils %}"
        "{% query 'boo'|query_set None %}"
    ).render(context)

    assert out == "foo=2&baz=3"


def test_query_toggle_set():
    request = MagicMock()
    request.GET = {'boo': '123', 'foo': 2}

    context = Context({
        'request': request
    })

    out = Template(
        "{% load query_utils %}"
        "{% query 'boo'|query_toggle '123' %}"
    ).render(context)

    assert out == "foo=2"


def test_query_toggle_unset():
    request = MagicMock()
    request.GET = {'foo': 2}

    context = Context({
        'request': request
    })

    out = Template(
        "{% load query_utils %}"
        "{% query 'boo'|query_toggle '123' %}"
    ).render(context)

    assert out == "foo=2&boo=123"