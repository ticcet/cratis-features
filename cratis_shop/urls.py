from cratis_shop.views import (
    SearchFrontendView, CheckoutView, CheckoutDoneView, ProductView,
    CompareProductsView, FrontPageView, ProductDataView, SearchFrontendDataView, SearchDebugView)
from django.conf.urls import url, patterns
from cratis_i18n.utils import localize_url as _

urlpatterns = patterns('',
                       url(_(r'^$'), FrontPageView.as_view(),
                           name='front_page'),
                       url(_(r'^search/$'), SearchFrontendView.as_view(), name='shop_search'),
                       url(_(r'^search/debug$'), SearchDebugView.as_view(), name='shop_debug'),
                       url(_(r'^checkout/$'), CheckoutView.as_view(),
                           name='shop_checkout'),
                       url(_(r'^checkout/done/$'), CheckoutDoneView.as_view(),
                           name='shop_checkout_done'),
                       url(_(r'^category/(?P<id>\w+)/$'),
                           SearchFrontendView.as_view(), name='shop_category'),

                       url(_(r'^product/(?P<id>\w+)/$'),
                           ProductView.as_view(), name='shop_product'),

                       url(_(r'^product_data/(?P<id>\w+)$'),
                           ProductDataView.as_view(), name='shop_product_data'),

                       url(_(r'^products-compare/$'), CompareProductsView.as_view(),
                           name='shop_products_compare'),

                       url(_(r'^category/(?P<parent_id>\w+)/(?P<id>\w+)/$'),
                           SearchFrontendView.as_view(), name='shop_sub_category'),

                       url(_(r'^search_data$'), SearchFrontendDataView.as_view(), name='search_data')
)

