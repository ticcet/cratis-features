import re
import os, glob
from setuptools import setup, find_packages, Command



def parse_requirements(file_name):
    requirements = []
    for line in open(file_name, 'r').read().split('\n'):
        if re.match(r'(\s*#)|(\s*$)', line):
            continue
        if re.match(r'\s*-e\s+', line):
            # TODO support version numbers
            requirements.append(re.sub(r'\s*-e\s+.*#egg=(.*)$', r'\1', line))
        elif re.match(r'\s*-f\s+', line):
            pass
        else:
            requirements.append(line)

    return requirements

def parse_dependency_links(file_name):
    dependency_links = []
    for line in open(file_name, 'r').read().split('\n'):
        if re.match(r'\s*-[ef]\s+', line):
            dependency_links.append(re.sub(r'\s*-[ef]\s+', '', line))

    return dependency_links


def parse_extras():
    extras = {}

    base_path = os.path.dirname(os.path.realpath(__file__))

    for file in glob.glob(base_path + '/cratis_*/'):
        name = re.match('.*/cratis_([^/]+)', file).groups()[0]
        file_name = '%s/requirements.txt' % file
        if os.path.exists(file_name):
            requirements = parse_requirements(file_name)
        else:
            requirements = []

        extras[name] = requirements

    return extras

class ReqCommand(Command):
    user_options = []

    def initialize_options(self):
        self.cwd = None
    def finalize_options(self):
        self.cwd = os.getcwd()

    def run(self):
        extras = parse_extras()
        print(extras)



setup(
    name='cratis-features',
    version='0.1.11',
    packages=find_packages(),

    url='',
    license='MIT',
    author='Alex Rudakov',
    author_email='ribozz@gmail.com',
    description='Integrates django-cms into cratis application',
    long_description='',
    install_requires=[
        'django',
        'django-cratis',
        'setuptools>17.1',
        'voluptuous',
        'PyYaml',
        # 'django-mptt==0.5.2',
        # 'django-cms<3.0',
        # 'Pillow',


        # suit
        #
        # 'django-suit',
        #
        #
        # 'Pillow',
        # 'easy-thumbnails',
        # 'Pillow',
        # 'django-filer',
        # 'cmsplugin-filer',
        # 'voluptuous',
    ],

    extras_require=parse_extras(),

    cmdclass={
        'req': ReqCommand
    }
)

