***********************
Base features
***********************

Common
===============

Feature installs following applications:

- django.contrib.auth
- django.contrib.contenttypes
- django.contrib.sessions
- django.contrib.messages
- django.contrib.staticfiles

And sets following standard Django middleware:

- django.contrib.sessions.middleware.SessionMiddleware
- django.middleware.common.CommonMiddleware
- django.middleware.csrf.CsrfViewMiddleware
- django.contrib.auth.middleware.AuthenticationMiddleware
- django.contrib.messages.middleware.MessageMiddleware
- django.middleware.clickjacking.XFrameOptionsMiddleware

In case of debug, extension also register media and static urls.
