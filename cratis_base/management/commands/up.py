import socket
from django.core.management.base import BaseCommand, CommandError
from django.core.management.commands.runserver import Command as RunCommand


class Command(RunCommand):
    help = 'Start cratis application in development mode'

    def handle(self, addrport='', *args, **options):

        if addrport == '':
            addrport = '%s:8000' % socket.gethostbyname(socket.gethostname())

        return super(Command, self).handle(addrport, *args, **options)



