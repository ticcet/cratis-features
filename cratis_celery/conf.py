
from __future__ import absolute_import

from cratis.cli import load_env

import os

os.environ.setdefault("CRATIS_APP_PATH", os.getcwd())
os.environ.setdefault('DJANGO_CONFIGURATION', 'Prod')
load_env()

from configurations import importer
importer.install()


from celery import Celery

from django.conf import settings

print(os.environ.get('DJANGO_CONFIGURATION'))

app = Celery('cratis')


# Using a string here means the worker will not have to
# pickle the object when using Windows.
app.config_from_object('django.conf:settings')

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)




@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))