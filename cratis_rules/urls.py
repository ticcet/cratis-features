from django.conf.urls import url, patterns, include
from .views import rule_data, RulePreviewView

urlpatterns = patterns('',

       url(r'^cratis_rules/rule_(?P<id>\d+).json$', rule_data, name='cratis_rules__rule'),
       url(r'^cratis_rules/preview/(?P<id>\d+)$', RulePreviewView.as_view(), name='cratis_rules__preview')

)
