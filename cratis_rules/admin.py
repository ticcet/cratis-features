from cratis_rules.models import Rule, RuleTypeValueIn, RuleTypeValueOut, RuleType
from django.contrib import admin
from django.contrib.admin.widgets import AdminTextareaWidget
from django.forms import Textarea, Select, ModelForm
from django.utils.safestring import mark_safe


class RuleWidget(AdminTextareaWidget):
    def render(self, name, value, attrs=None):

        return super(RuleWidget, self).render(name, value, attrs) + mark_safe("""

            <div id="blocklyDiv" style="height: 600px; width: 850px;"></div>

            <!-- <h3>Try yourself</h3>

            <div class="form-horizontal" id="try-form">
              <div id="try">
              </div>
            </dvi>

            <h4>Result</h4>
            <pre><code id="result"></code></pre> -->

            <h4>Generated code</h4>
            <pre><code id="blocklyCode"></code></pre>
            <pre><code id="blocklyCodePython"></code></pre>

            <xml id="toolbox" style="display: none">

                <category name="= Input =">
                    <block type="variables_set">
                        <field name="VAR">result</field>
                    </block>
                </category>

                <category name="= Result =">
                    <block type="variables_get">
                        <field name="VAR">result</field>
                    </block>
                </category>

                <category name="Variables" custom="VARIABLE">

                </category>
                <category name="Functions" custom="PROCEDURE"></category>

                <category name="Logic">
                  <block type="controls_if"></block>
                  <block type="logic_compare"></block>
                  <block type="logic_operation"></block>
                  <block type="logic_negate"></block>
                  <block type="logic_boolean"></block>
                </category>
                <category name="Loops">
                  <block type="controls_repeat_ext">
                    <value name="TIMES">
                      <block type="math_number">
                        <field name="NUM">10</field>
                      </block>
                    </value>
                  </block>
                  <block type="controls_whileUntil"></block>
                  <block type="controls_for">
                    <field name="VAR">i</field>
                    <value name="FROM">
                      <block type="math_number">
                        <field name="NUM">1</field>
                      </block>
                    </value>
                    <value name="TO">
                      <block type="math_number">
                        <field name="NUM">10</field>
                      </block>
                    </value>
                    <value name="BY">
                      <block type="math_number">
                        <field name="NUM">1</field>
                      </block>
                    </value>
                  </block>
                </category>
                <category name="Math">
                  <block type="math_number"></block>
                  <block type="math_arithmetic"></block>
                  <block type="math_single"></block>
                </category>
                <category name="Text">
                  <block type="text"></block>
                  <block type="text_length"></block>
                  <block type="text_print"></block>
                </category>
            </xml>""" % {'id': attrs['id']})


class RuleAdminForm(ModelForm):
    class Meta:
        model = Rule

        widgets = {
            'rule': RuleWidget(attrs={'class': 'cratis-rule-field'})
        }


class RulesAdmin(admin.ModelAdmin):
    form = RuleAdminForm

    save_as = True

    fields = ('name', 'rule_type', 'rule')

    class Media:
        css = {
            "all": ('bower/chosen_v1.1.0/chosen.min.css',)
        }
        js = (
            "blockly/blockly_compressed.js",
            "blockly/blocks_compressed.js",
            "blockly/blocks_compressed.js",
            "blockly/javascript_compressed.js",
            "blockly/python_compressed.js",
            "blockly/msg/js/en.js",
            "js/cratis-rule.js",
        )


admin.site.register(Rule, RulesAdmin)


class InlineRuleTypeValueIn(admin.TabularInline):
    model = RuleTypeValueIn
    extra = 3

class InlineRuleTypeValueOut(admin.TabularInline):
    model = RuleTypeValueOut
    extra = 3

class RulesTypeAdmin(admin.ModelAdmin):
    inlines = [InlineRuleTypeValueIn, InlineRuleTypeValueOut]

admin.site.register(RuleType, RulesTypeAdmin)
