# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Rules'
        db.delete_table(u'cratis_rules_rules')

        # Adding model 'Rule'
        db.create_table(u'cratis_rules_rule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('rule', self.gf('django.db.models.fields.TextField')()),
            ('rule_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cratis_rules.RuleType'], null=True)),
        ))
        db.send_create_signal(u'cratis_rules', ['Rule'])


    def backwards(self, orm):
        # Adding model 'Rules'
        db.create_table(u'cratis_rules_rules', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rule', self.gf('django.db.models.fields.TextField')()),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
        ))
        db.send_create_signal(u'cratis_rules', ['Rules'])

        # Deleting model 'Rule'
        db.delete_table(u'cratis_rules_rule')


    models = {
        u'cratis_rules.rule': {
            'Meta': {'object_name': 'Rule'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cratis_rules.RuleType']", 'null': 'True'})
        },
        u'cratis_rules.ruletype': {
            'Meta': {'object_name': 'RuleType'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'cratis_rules.ruletypevaluein': {
            'Meta': {'object_name': 'RuleTypeValueIn'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'in_vars'", 'to': u"orm['cratis_rules.RuleType']"})
        },
        u'cratis_rules.ruletypevalueout': {
            'Meta': {'object_name': 'RuleTypeValueOut'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'out_vars'", 'to': u"orm['cratis_rules.RuleType']"})
        }
    }

    complete_apps = ['cratis_rules']