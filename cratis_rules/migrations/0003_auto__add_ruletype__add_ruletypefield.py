# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RuleType'
        db.create_table(u'cratis_rules_ruletype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'cratis_rules', ['RuleType'])

        # Adding M2M table for field in_vars on 'RuleType'
        m2m_table_name = db.shorten_name(u'cratis_rules_ruletype_in_vars')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ruletype', models.ForeignKey(orm[u'cratis_rules.ruletype'], null=False)),
            ('ruletypefield', models.ForeignKey(orm[u'cratis_rules.ruletypefield'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ruletype_id', 'ruletypefield_id'])

        # Adding M2M table for field out_vars on 'RuleType'
        m2m_table_name = db.shorten_name(u'cratis_rules_ruletype_out_vars')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ruletype', models.ForeignKey(orm[u'cratis_rules.ruletype'], null=False)),
            ('ruletypefield', models.ForeignKey(orm[u'cratis_rules.ruletypefield'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ruletype_id', 'ruletypefield_id'])

        # Adding model 'RuleTypeField'
        db.create_table(u'cratis_rules_ruletypefield', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('var_type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'cratis_rules', ['RuleTypeField'])


    def backwards(self, orm):
        # Deleting model 'RuleType'
        db.delete_table(u'cratis_rules_ruletype')

        # Removing M2M table for field in_vars on 'RuleType'
        db.delete_table(db.shorten_name(u'cratis_rules_ruletype_in_vars'))

        # Removing M2M table for field out_vars on 'RuleType'
        db.delete_table(db.shorten_name(u'cratis_rules_ruletype_out_vars'))

        # Deleting model 'RuleTypeField'
        db.delete_table(u'cratis_rules_ruletypefield')


    models = {
        u'cratis_rules.rules': {
            'Meta': {'object_name': 'Rules'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {})
        },
        u'cratis_rules.ruletype': {
            'Meta': {'object_name': 'RuleType'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_vars': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'in_for'", 'symmetrical': 'False', 'to': u"orm['cratis_rules.RuleTypeField']"}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'out_vars': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'out_for'", 'symmetrical': 'False', 'to': u"orm['cratis_rules.RuleTypeField']"})
        },
        u'cratis_rules.ruletypefield': {
            'Meta': {'object_name': 'RuleTypeField'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'var_type': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['cratis_rules']