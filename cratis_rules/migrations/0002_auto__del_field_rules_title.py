# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Rules.title'
        db.delete_column(u'cratis_rules_rules', 'title')


    def backwards(self, orm):
        # Adding field 'Rules.title'
        db.add_column(u'cratis_rules_rules', 'title',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)


    models = {
        u'cratis_rules.rules': {
            'Meta': {'object_name': 'Rules'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['cratis_rules']