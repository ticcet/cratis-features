# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing M2M table for field out_vars on 'RuleType'
        db.delete_table(db.shorten_name(u'cratis_rules_ruletype_out_vars'))

        # Removing M2M table for field in_vars on 'RuleType'
        db.delete_table(db.shorten_name(u'cratis_rules_ruletype_in_vars'))


    def backwards(self, orm):
        # Adding M2M table for field out_vars on 'RuleType'
        m2m_table_name = db.shorten_name(u'cratis_rules_ruletype_out_vars')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ruletype', models.ForeignKey(orm[u'cratis_rules.ruletype'], null=False)),
            ('ruletypefield', models.ForeignKey(orm[u'cratis_rules.ruletypefield'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ruletype_id', 'ruletypefield_id'])

        # Adding M2M table for field in_vars on 'RuleType'
        m2m_table_name = db.shorten_name(u'cratis_rules_ruletype_in_vars')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('ruletype', models.ForeignKey(orm[u'cratis_rules.ruletype'], null=False)),
            ('ruletypefield', models.ForeignKey(orm[u'cratis_rules.ruletypefield'], null=False))
        ))
        db.create_unique(m2m_table_name, ['ruletype_id', 'ruletypefield_id'])


    models = {
        u'cratis_rules.rules': {
            'Meta': {'object_name': 'Rules'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {})
        },
        u'cratis_rules.ruletype': {
            'Meta': {'object_name': 'RuleType'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'cratis_rules.ruletypefield': {
            'Meta': {'object_name': 'RuleTypeField'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'var_type': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['cratis_rules']