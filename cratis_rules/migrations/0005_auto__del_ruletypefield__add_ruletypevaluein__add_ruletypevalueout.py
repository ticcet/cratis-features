# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'RuleTypeField'
        db.delete_table(u'cratis_rules_ruletypefield')

        # Adding model 'RuleTypeValueIn'
        db.create_table(u'cratis_rules_ruletypevaluein', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rule_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='in_vars', to=orm['cratis_rules.RuleType'])),
        ))
        db.send_create_signal(u'cratis_rules', ['RuleTypeValueIn'])

        # Adding model 'RuleTypeValueOut'
        db.create_table(u'cratis_rules_ruletypevalueout', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rule_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='out_vars', to=orm['cratis_rules.RuleType'])),
        ))
        db.send_create_signal(u'cratis_rules', ['RuleTypeValueOut'])


    def backwards(self, orm):
        # Adding model 'RuleTypeField'
        db.create_table(u'cratis_rules_ruletypefield', (
            ('var_type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'cratis_rules', ['RuleTypeField'])

        # Deleting model 'RuleTypeValueIn'
        db.delete_table(u'cratis_rules_ruletypevaluein')

        # Deleting model 'RuleTypeValueOut'
        db.delete_table(u'cratis_rules_ruletypevalueout')


    models = {
        u'cratis_rules.rules': {
            'Meta': {'object_name': 'Rules'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'rule': ('django.db.models.fields.TextField', [], {})
        },
        u'cratis_rules.ruletype': {
            'Meta': {'object_name': 'RuleType'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'})
        },
        u'cratis_rules.ruletypevaluein': {
            'Meta': {'object_name': 'RuleTypeValueIn'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'in_vars'", 'to': u"orm['cratis_rules.RuleType']"})
        },
        u'cratis_rules.ruletypevalueout': {
            'Meta': {'object_name': 'RuleTypeValueOut'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rule_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'out_vars'", 'to': u"orm['cratis_rules.RuleType']"})
        }
    }

    complete_apps = ['cratis_rules']