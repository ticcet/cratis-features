$(function () {
    var textarea = $('.cratis-rule-field');

    if (textarea) {
        textarea.hide();


        Blockly.inject(document.getElementById('blocklyDiv'),
            {path: '/static/blockly/', toolbox: document.getElementById('toolbox')});

        function loadTools(rule_type) {
            if (!rule_type) {
                return;
            }
            $.get('/cratis_rules/rule_' + rule_type + '.json', function(data) {

                var vars = '';
                var form = '';
                $.each( data.fields.in, function( index, field){
                    vars += '<block type="variables_get"><field name="VAR">' + field.name + '</field></block>';

                    form += '<div class="control-group"><label class="control-label" for="input' + field.name + '">' + field.name + '</label><div class="controls"><input name="' + field.name + '" type="text" id="input' + field.name + '" placeholder="' + field.var_type + '"><span class="help-block">' + field.description + '</span></div></div>';

                });
                $('#toolbox category[name="= Input ="]').html(vars);

                //$('#try').html(form);

                var vars = '';
                $.each( data.fields.out, function( index, field){
                    vars += '<block type="variables_set"><field name="VAR">' + field.name + '</field></block>'
                });
                $('#toolbox category[name="= Result ="]').html(vars);

                Blockly.updateToolbox(document.getElementById('toolbox'));
            });
        }

        function calculateResult() {

        }

        function myUpdateFunction() {

            var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
            var xml_text = Blockly.Xml.domToText(xml);

            var jsCode = Blockly.JavaScript.workspaceToCode();
            var pythonCode = Blockly.Python.workspaceToCode();

            $('#blocklyCode').html(jsCode);
            $('#blocklyCodePython').html(pythonCode);

            var json = JSON.stringify({xml: xml_text, js: jsCode, python: pythonCode});
            $('.cratis-rule-field').val(json);
        }

        try {
            var data = JSON.parse($('.cratis-rule-field').val());
            if (data.xml) {
                var xml = Blockly.Xml.textToDom(data.xml);
                Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
            }

            $('#id_rule_type').change(function() {
                loadTools($(this).val());
            });

            if ($('#id_rule_type').val()) {

                loadTools($('#id_rule_type').val());
            }

            $('#try').on('input', 'keypress', function(e) {

                if (e.which == 13) {
                e.preventDefault();
                $('form#login').submit();
              }
            });


        } catch (e) {
        }

        Blockly.addChangeListener(myUpdateFunction);
    }
});