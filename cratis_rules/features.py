from django.conf.urls import patterns, include
import os
from cratis.features import Feature


class RulesEngine(Feature):

    def configure_settings(self):
        self.append_apps(['cratis_rules'])


        self.settings.STATICFILES_DIRS += (os.path.dirname(__file__) + '/static',)


    def configure_urls(self, urls):
        urls += patterns('',
                         (r'^', include('cratis_rules.urls')),
                     )